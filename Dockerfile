#FROM node:13.8.0-alpine3.10
FROM node:13.8.0-alpine3.10

WORKDIR /usr/api

COPY package*.json ./

RUN yarn && yarn cache clean

COPY . .