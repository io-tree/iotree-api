

module.exports.pathRoutes = '../routes';

module.exports.pathControllers = '../controllers';

module.exports.pathSchedulers = '../schedulers';

module.exports.pathApi = '/api/v1';