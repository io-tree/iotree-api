'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('react_types', {
			id_react_type: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			name: {
				type: Sequelize.STRING
			},
			path_icon: {
				type: Sequelize.STRING,
				defaultValue: null
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('react_types');
	}
};