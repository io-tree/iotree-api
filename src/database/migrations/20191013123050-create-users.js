'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('users', {
			id_user: {
				type: Sequelize.UUID,
				defaultValue: Sequelize.UUIDV4,
				primaryKey: true,
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			email: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false
			},
			birthday_date: {
				type: Sequelize.DATEONLY,
				allowNull: false
			},
			id_gender: {
				type: Sequelize.INTEGER,
				references: {
					model: 'genders',
					key: 'id_gender',
					onDelete: 'SET NULL'
				},
				defaultValue: 0
			},
			is_admin: {
				type: Sequelize.BOOLEAN,
				defaultValue: false
			},
			path_img: {
				type: Sequelize.STRING,
				defaultValue: '/upload/default-user.png'
			},
			login_attempts: {
				type: Sequelize.INTEGER,
				defaultValue: 0
			},
			is_enabled: {
				type: Sequelize.BOOLEAN,
				defaultValue: false,
				allowNull: false
			},
			passwordChangedAt: {
				type: Sequelize.DATE,
			},
			blockedAt: {
				type: Sequelize.DATE,
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('users');
	}
};