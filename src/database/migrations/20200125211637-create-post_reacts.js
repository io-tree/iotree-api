'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('post_reacts', {
			id_react: {
				type: Sequelize.UUID,
				defaultValue: Sequelize.UUIDV4,
				primaryKey: true,
			},
			id_post: {
				type: Sequelize.UUID,
				allowNull: false,
				references: {
					model: 'posts',
					key: 'id_post'
				},
			},
			id_user: {
				type: Sequelize.UUID,
				references: {
					model: 'users',
					key: 'id_user'
				},
			},
			id_react_type: {
				type: Sequelize.INTEGER,
				references: {
					model: 'react_types',
					key: 'id_react_type'
				},
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('post_reacts');
	}
};