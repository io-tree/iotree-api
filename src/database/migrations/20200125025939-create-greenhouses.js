'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('greenhouses', {
			id_greenhouse: {
				type: Sequelize.UUID,
				defaultValue: Sequelize.UUIDV4,
				primaryKey: true,
			},
			id_user: {
				type: Sequelize.UUID,
				references: {
					model: 'users',
					key: 'id_user'
				},
			},
			code: {
				type: Sequelize.STRING,
				unique: true
			},
			id_type: {
				type: Sequelize.STRING,
				references: {
					model: 'greenhouse_types',
					key: 'id_greenhouse_type'
				},
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			online:{
				type: Sequelize.BOOLEAN,
				allowNull: false,
				defaultValue: false
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('greenhouses');
	}
};