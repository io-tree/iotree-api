'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('plantations', {
      id_plantation: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      id_plant: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'plants',
          key: 'id_plant'
        },
      },
      id_greenhouse: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'greenhouses',
          key: 'id_greenhouse'
        },
      },
      id_user: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id_user'
        },
      },
      is_done: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      temperature: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ''
      },
      humidity_air: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ''
      },
      humidity_ground: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: ''
      },
      time_irrigation: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      done_time: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('plantations');
  }
};