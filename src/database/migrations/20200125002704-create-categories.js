'use strict';

module.exports = {
 	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('categories', {
			id_category: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			description: {
				type: Sequelize.TEXT
			},
			num_order: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			path_img: {
				type: Sequelize.STRING,
				defaultValue: '/upload/default-category.png'
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
  	},

  	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('categories');
  	}
};
