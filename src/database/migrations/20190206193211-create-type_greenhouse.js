'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('greenhouse_types', {
			id_greenhouse_type: {
				type: Sequelize.STRING(3),
				primaryKey: true,
				allowNull: false
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			num_order: { 
				type: Sequelize.INTEGER,
				allowNull: false
			},
			description: {
				type: Sequelize.TEXT
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('greenhouse_types');
	}
};
