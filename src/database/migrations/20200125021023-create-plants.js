'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('plants', {
      id_plant: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      scientific_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT
      },
      humidity_air: {
        type: Sequelize.STRING,
        allowNull: false
      },
      humidity_ground: {
        type: Sequelize.STRING,
        allowNull: false
      },
      temperature: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      pH: {
        type: Sequelize.STRING
      },
      height: {
        type: Sequelize.STRING,
      },
      id_category: {
        type: Sequelize.INTEGER,
        references: {
          model: 'categories',
          key: 'id_category'
        },
        allowNull: false
      },
      family: {
        type: Sequelize.STRING,
      },
      origin: {
        type: Sequelize.STRING,
      },
      cycle: {
        type: Sequelize.STRING,
        allowNull: false
      },
      luminosity: {
        type: Sequelize.STRING,
      },
      weather: {
        type: Sequelize.STRING,
      },
      path_img: {
        type: Sequelize.STRING,
        defaultValue: '/upload/default-plant.png'
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('plants');
  }
};