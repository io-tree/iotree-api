'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('comment_reacts', {
			id_comment_react: {
				type: Sequelize.UUID,
				defaultValue: Sequelize.UUIDV4,
				primaryKey: true,
			},
			id_comment: {
				type: Sequelize.UUID,
				allowNull: false,
				references: {
					model: 'comments',
					key: 'id_comment'
				},
			},
			id_user: {
				type: Sequelize.UUID,
				references: {
					model: 'users',
					key: 'id_user'
				},
			},
			id_react_type: {
				type: Sequelize.INTEGER,
				references: {
					model: 'react_types',
					key: 'id_react_type'
				},
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('comment_reacts');
	}
};