'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('comments', {
			id_comment: {
				type: Sequelize.UUID,
				defaultValue: Sequelize.UUIDV4,
				primaryKey: true,
			},
			id_user: {
				type: Sequelize.UUID,
				allowNull: false,
				references: {
					model: 'users',
					key: 'id_user'
				},
			},
			id_post: {
				type: Sequelize.UUID,
				allowNull: false,
				references: {
					model: 'posts',
					key: 'id_post'
				},
			},
			in_comment: {
				type: Sequelize.UUID,
				references: {
					model: 'comments',
					key: 'id_comment'
				},
			},
			text: {
				type: Sequelize.TEXT
			},
			createdAt: {
				type: Sequelize.DATE,
				allowNull: false
			},
			updatedAt: {
				type: Sequelize.DATE,
				allowNull: false
			}
		})
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('comments');
	}
};