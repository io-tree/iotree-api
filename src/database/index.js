const Sequelize = require('sequelize');

const { test, development, production } = require('./config');


const sequelize = new Sequelize(
	process.env.NODE_ENV === 'test' ? test : process.env.NODE_ENV === 'development' ? development : production
);

sequelize.Op = Sequelize.Op;
sequelize.sequelize = sequelize;

module.exports = sequelize;