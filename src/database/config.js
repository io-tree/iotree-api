
module.exports = {
	test: {
		database: 'test',
		storage: 'test.sqlite',
		dialect: 'sqlite',
		seederStorage: 'sequelize',
		dialectOptions: {
			dateStrings: true,
			typeCast: true
		},
		define: {
			timestamps: true
		},
		logging: false,
	},
	development: {
		database: 'development',
		dialect: 'sqlite',
		storage: 'development.sqlite',
		seederStorage: 'sequelize',
		dialectOptions: {
			dateStrings: true,
			typeCast: true
		},
		define: {
			timestamps: true
		},
		logging: false
	},
	production: {
		host: process.env.HOST,
		database: 'production',
		username: process.env.USER,
		password: process.env.PASSWORD,
		dialect: 'postgres',
		seederStorage: 'sequelize',
		port: 15432,
		dialectOptions: {
			dateStrings: true,
			typeCast: true
		},
		define: {
			timestamps: true,
		},
		timezone: '-03:00', //for writing to database
		logging: false
	},
	production_dev: {
		host: process.env.HOST,
		database: 'production',
		username: process.env.USER,
		password: process.env.PASSWORD,
		seederStorage: 'sequelize',
		dialect: 'postgres',
		port: 5432,
		dialectOptions: {
			dateStrings: true,
			typeCast: true
		},
		define: {
			timestamps: true,
			paranoid: true
		},
		timezone: '-03:00' //for writing to database
	}
}