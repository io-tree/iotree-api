'use strict';

const uuidv4 = require('uuid/v4');


module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('plants', [
      {
        id_plant: uuidv4(),
        name: 'Rabanete',
        scientific_name: 'Raphanus sativus L',
        description: 'O rabanete é uma planta que produz uma raiz muito saborosa e bastante consumida, principalmente crua e em saladas.',
        humidity_air: '{"min": 40, "max": 60, "unid": "%"}',
        humidity_ground: '{"min": 40, "max": 60, "unid": "%"}',
        cycle: '{"min": 20, "max": 25, "tolerance": 10, "unid": "dia"}',
        temperature: '{"min": 10, "max": 20, "unid": "°C"}',
        pH: '{"min": 6.6, "max": 7.5, "unid": "pH"}',
        id_category: 1,
        family: 'Brassicaceae',
        height: '{"min": 8, "max": 20, "unid": "cm"}',
        weather: 'fio ou temprerado',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('plants', null, {});
  }
};