'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('greenhouse_types', [
			{ id_greenhouse_type: 'DFL', name: 'Padrão', num_order: 1, description: 'Tipo padrão de estufa', createdAt: new Date(), updatedAt: new Date() },
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('greenhouse_types', null, {});
	}
};
