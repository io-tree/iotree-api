'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('genders', [
			{ name: 'Outros', createdAt: new Date(), num_order: 3, updatedAt: new Date() },
			{ name: 'Masculino', createdAt: new Date(),	num_order: 1, updatedAt: new Date() },
			{ name: 'Feminino', createdAt: new Date(),	num_order: 2, updatedAt: new Date() },
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('genders', null, {});
	}
};
