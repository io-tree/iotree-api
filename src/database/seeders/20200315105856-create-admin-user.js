'use strict';
const uuidv4 = require('uuid/v4');
const { createHash } = require('../../app/helpers/crypt');

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('users', [{ 
			id_user: uuidv4(),
			name: 'Administrador', 
			email: 'admin@admin.com',
			password: createHash('admin123'),
			birthday_date: new Date(),
			id_gender: 3,
			is_enabled: true,
			is_admin: true,
			createdAt: new Date(),	
			updatedAt: new Date() 
		}], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('users', null, {});
	}
};
