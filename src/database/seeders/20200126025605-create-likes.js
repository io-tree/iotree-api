'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('react_types', [{
				name: 'Curtir',
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: 'Não curtir',
				createdAt: new Date(),
				updatedAt: new Date()
			}
		], {});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('react_types', null, {});
	}
};