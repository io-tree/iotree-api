const path = require('path');
const { readdirSync } = require('fs');
const { pathSchedulers } = require('../../config/paths');

module.exports = (app) => {
	readdirSync(path.join(__dirname, pathSchedulers))
		.filter( fileName => (fileName !== 'index.js'))
		.forEach(fileName => {
			console.log(fileName);
			require(path.join(__dirname,pathSchedulers, path.basename(fileName)));
		});
};
