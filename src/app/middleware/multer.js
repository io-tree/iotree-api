const _multer = require('multer');
const path = require('path');

const multer = (_multer({
	storage: _multer.diskStorage({
		destination: (req, file, cb) => {
			cb(null, path.join(path.dirname(require.main.filename), 'public/upload/temp'));
		},

		filename: (req, file, cb) => {
			cb(null, Date.now().toString() + '-' + file.originalname);
		},


	}),

	fileFilter: (req, file, cb) => {
		const isAccepted = [
			'image/png',
			'image/jpg',
			'image/jpeg',
			//'application/octet-stream',
		];

		console.log(file.mimetype)
		
		if (isAccepted.includes(file.mimetype)) {
			return cb(null, true);
		}

		return cb(null, false);
	},


}));

module.exports = {
	multer
}