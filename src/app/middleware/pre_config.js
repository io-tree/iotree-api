const {
	genders,
	categories,
	greenhouse_types
} = require('../models');

const {
	decrypt
} = require('../helpers/crypt');

const { sendResponse } = require('../middleware/result');

const {
	languages
} = require('../helpers/translate');

const pre_config = async (req, res, next) => {

	if (process.env.SECURITY_ENABLED === 'true') {
		console.log(req.body)
		if (req.body && req.body.key) {
			try {
				req.body = JSON.parse(decrypt(req.body.key));
			} catch (err) {
				req.payload = {
					status: 'error',
					msg: 'badRequest'
				}
				return sendResponse(req, res)
			}
		} else {
			req.payload = {
				status: 'error',
				msg: 'badRequest'
			}
			return sendResponse(req, res)
		}
	}

	if (req.headers.language) {
		let temp = req.headers.language.toLowerCase();
		let codes = languages.map( lang => lang.code);
		
		if(codes.includes(temp)) {
			req.language = temp;
		}
	} else {
		req.language =  process.env.DEFAULT_LANGUAGE;
	}

	let all_genders = await genders.findAll({ raw: true });
	let all_categories = await categories.findAll({ raw: true });
	let all_greenhouse_types = await greenhouse_types.findAll({ raw: true });

	req.genders = all_genders.map(gender => gender.id_gender);
	req.genders_order = all_genders.map(gender => gender.num_order);
	
	req.greenhouse_types = all_greenhouse_types.map( greenhouse_type => greenhouse_type.id_greenhouse_type);
	req.greenhouse_types_order = all_greenhouse_types.map( greenhouse_type => greenhouse_type.num_order);

	req.categories = all_categories.map(category => category.id_category);
	req.categories_order = all_categories.map(category => category.num_order);

	return next();
}

module.exports = {
	pre_config
}