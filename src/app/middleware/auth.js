const {
  users,
  plantations,
  genders
} = require('../models');
const {
  sendResponse
} = require('./result');
const {
  verifyToken,
  generateToken
} = require('../helpers/token')

const auth = async (req, res, next) => {
  if (req.headers.authorization && req.headers['refresh-token']) {
    let refreshToken = req.headers['refresh-token'];
    let [bearer, token] = req.headers.authorization.split(' ');
    let decode = verifyToken(token);

    if (bearer !== 'Bearer' || !decode) {

      const decodeRefreshToken = verifyToken(refreshToken);

      if (decodeRefreshToken && decodeRefreshToken.token === token) {
        let id = decodeRefreshToken.id;
        token = generateToken({
          id
        });
        refreshToken = generateToken({
          id,
          token
        }, true);
        decode = {
          id
        };

      } else {
        req.payload = {
          status: 'error',
          msg: 'tokenInvalid'
        }

        return sendResponse(req, res);
      }

    }

    let user = await users.findOne({
      where: {
        id_user: decode.id,
        //is_admin
      },
      include: [{
        model: plantations,
        as: 'plantations'
      }, {
        model: genders,
        as: 'gender',
        attributes: ['name']
      }],
    });

    if (!user) {
      req.payload = {
        status: 'error',
        msg: 'userNotExist'
      }

      return sendResponse(req, res);
    }

    if (user && !user.is_admin && req.params.idUser !== decode.id) {
      req.payload = {
        status: 'error',
        msg: 'userNotAuthorization'
      }

      return sendResponse(req, res);
    }

    if (user) {
      req.user = user

      req.token = token;
      req.refreshToken = refreshToken;

      return next();

    } else {

      req.payload = {
        status: 'error',
        msg: 'userNotExist'
      }

    }
  } else {
    req.payload = {
      status: 'error',
      msg: 'tokenNotExist'
    }

  }
  sendResponse(req, res);
};

module.exports = {
  auth
}