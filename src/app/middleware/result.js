const {
	message
} = require('../helpers/translate');

const {
	encrypt,
	decrypt
} = require('../helpers/crypt');


const sendResponse = (req, res) => {
	if (req.payload.msg === undefined) {
		req.payload.msg = '';
		req.payload.code = '';
	} else if (req.payload.msg === 'validationError' && req.payload.body) {
		req.payload.body = req.payload.body.map( error => {
			return error[req.language]
		})
		req.payload.code = req.payload.msg;
	} else {
		req.payload.code = req.payload.msg;
		req.payload.msg = message[req.payload.msg][req.language];
	}

	let payload = null;
	
	if (process.env.SECURITY_ENABLED === 'true'){
		const key = encrypt(JSON.stringify(req.payload));
		payload = {
			key: key,
			token: req.token,
			refreshToken: req.refreshToken
		}
	} else {
		req.payload.token = req.token;
		req.payload.refreshToken = req.refreshToken;
		payload = req.payload;
	}
	return res.send(payload);
};

module.exports = {
	sendResponse
}