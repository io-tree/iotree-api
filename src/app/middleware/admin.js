const {
	sendResponse
} = require('./result');

const adminRoute = async (req, res, next) => {
	if (!req.user.is_admin) {
		console.log('não é admin')
		req.payload = {
			status: 'error',
			msg: 'userNotAuthorization'
		}
		return sendResponse(req, res);
	}

	next();
}

module.exports = {
	adminRoute
}