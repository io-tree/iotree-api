const words = require('../helpers/translate');

const getWord = (req, res, next) => {
	req.payload = {
		status: 'ok',
		body: words
	}

	next();
}

module.exports = {
	getWord
}