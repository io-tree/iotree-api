const {
	post_reacts,
	comment_reacts,
	posts,
	comments
} = require('../models');

const createReact = async (req, res, next) => {
	const {
		id,
		type_react,
		target
	} = req.body;

	let react = {};
	let mod = null;
	let mod_2 = null;
	let idd = null;

	if (target == 'comment') {
		mod = comment_reacts;
		mod_2 = comments;
		idd = 'id_comment';
	} else if (target == 'post') {
		mod = post_reacts;
		mod_2 = posts;
		idd = 'id_post';
	}

	let exs = await mod_2.findOne({
		where: {
			[idd]: id
		}
	});

	if (!exs) {
		req.payload = {
			status: 'error',
			msg: idd + ' não existe'
		}
		return next();
	}

	react = await mod.findOne({
		where: {
			[idd]: id,
			id_user: req.user.id_user
		}
	});

	if (react) {
		react = await mod.update({
			id_react_type: type_react,
		}, {
			where: {
				[idd]: id,
				id_user: req.user.id_user
			}
		});
	} else {
		react = await mod.create({
			[idd]: id,
			id_react_type: type_react,
			id_user: req.user.id_user
		})
	}

	req.payload = {
		status: 'ok',
		body: react
	}

	next();
}

const deleteReact = async (req, res, next) => {
	const {
		id,
		target
	} = req.body;

	let react = {};
	let mod = null;
	let idd = null;

	if (target == 'comment') {
		mod = comment_reacts;
		idd = 'id_comment';
	} else if (target == 'post') {
		mod = post_reacts;
		idd = 'id_post';
	}

	react = await mod.findOne({
		where: {
			[idd]: id,
			id_user: req.user.id_user
		}
	});

	if (react) {
		react = await react.destroy();
		req.payload = {
			status: 'ok',
			msg: 'Reação excluida'
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'Reação não encontrada'
		}
	}


	next();
}

module.exports = {
	createReact,
	deleteReact
}