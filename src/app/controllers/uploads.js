const fs = require('fs');
const path = require('path');
var multer = require('multer')
const {
  sharp
} = require('../helpers');

const {
  posts,
  users,
  comments,
  categories,
  plants
} = require('../models');

const createArq = (req, res, next) => {

  function del () {
    req.files.map(fl => {
      sharp.deleteImg(fl)
    })
  }

  let path = req.body.path;
  let mod = null;
  let idd = null;
  let sufix = ''

  if (path === 'users') {
    mod = users;
    idd = 'id_user';
  } else if (path === 'posts') {
    mod = posts;
    idd = 'id_post';
  } else if (path === 'comments') {
    mod = comments;
    idd = 'id_comment';
  } else if (path === 'categories') {
    mod = categories
    idd = 'id_category'
  } else if (path === 'plants') {
    mod = plants
    idd = 'id_plant'
  }

  if (path == 'users') {
    if (req.files.length > 1) {
      req.payload = {
        status: 'error',
        msg: 'fileMany'
      }
      del()
      return next();
    }
  } else if (path == 'posts') {
    if (req.files.length > 5) {
      req.payload = {
        status: 'error',
        msg: 'fileMany'
      }
      del()
      return next();
    }

  }
  console.log(path)
  mod.findOne({
    where: {
      [idd]: req.body.id
    }
  }).then(arr => {
    if (arr) {
      Promise.all(req.files.map((fl, i) => {
        if (path == 'posts') {
          sufix = '_' + i;
          sharp.compressImage(fl, 100, path + '/' + req.body.id + sufix + '_small')
        }
        return sharp.compressImage(fl, null, path + '/' + req.body.id + sufix)
      })).then(async paths => {
        req.payload = {
          status: 'ok',
          msg: 'fileSaveSuccess',
          body: paths
        }

        if (path === 'users' || path === 'posts' || path === 'categories' || path === 'plants') {
          arr.path_img = paths.join()
          await arr.save();
        }

        next();
      }).catch(err => {
        req.payload = {
          status: 'error',
          msg: 'fileErroCompression'
        }
        next();
      })

    } else {
      req.payload = {
        status: 'error',
        msg: 'fileParamsIncorrect'
      }
      next();
    }
  }).catch(err => {
    req.payload = {
      status: 'error',
      msg: 'fileParamsIncorrect'
    }
    console.log(err)
    next();
  })

}

const deleteArq = async (req, res, next) => {

  const {
    id,
    mod
  } = req.query;

  let test = [
    'users',
    'posts',
    'comments',
    'categories'
  ].includes(mod);

  if (test) {
    try {
      let cam = path.dirname(require.main.filename) + '/public/upload/' + mod + '/' + id + '.webp';
      fs.accessSync(cam);
      fs.unlinkSync(cam);

      if (mod === 'users') {
        let tUser = await users.findOne({
          where: {
            id_user: id
          }
        });

        tUser.path_img = '/upload/default-user.png';
        await tUser.save();
      }

      req.payload = {
        status: 'ok',
        msg: 'fileDeletedSuccess'
      }
    } catch (e) {
      req.payload = {
        status: 'error',
        msg: 'fileNotExisted'
      }
    }
  } else {
    req.payload = {
      status: 'error',
      msg: 'fileParamsIncorrect'
    }
  }

  next();
}

module.exports = {
  createArq,
  deleteArq
}