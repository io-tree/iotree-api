let connectedUsers = {};
let _io = null;

const socket = (io) => {
  _io = io;
  io.on('connection', (socket) => {

    console.log("Socket Id:" + socket.id);

    setUser(socket.id);

    socket.on('verify_user', function (data) {
      console.log(data);
      if (data === 'ok') {
        socket.emit(socket.id, 'ok');
        connectedUsers[socket.id] = {
          id: '',
          name: ''
        }
        console.log(connectedUsers);
      }
    });

    socket.on('user_connect', (data) => {
      setUser(socket.id);
      connectedUsers[socket.id]['id'] = data.id_user;
      connectedUsers[socket.id]['name'] = data.name;
      console.log(connectedUsers);
    });

    socket.on('user_disconnect', () => {
      connectedUsers[socket.id]['id'] = '';
      connectedUsers[socket.id]['name'] = '';
      console.log(connectedUsers);
    });

    socket.on('my_other_event', (data) => {
      console.log(data);
    });

    socket.on('disconnect', function () {
      userDelete(socket.id);
    });
  })
}

const userDelete = (id) => {
  delete connectedUsers[id];
  console.log(connectedUsers);
  console.log('disconectado')
}

const setUser = (id) => {
  if (id && !Object.keys(connectedUsers).includes(id)) {
    connectedUsers[id] = {
      id: '',
      name: ''
    }
  }
  console.log(connectedUsers);
}

const setDataSensor = (id_user, values) => {
  const idSocket = getIdSocketUser(id_user);
  //console.log(idSocket, id_user, connectedUsers)
  if (idSocket) {
    _io.to(idSocket).emit('sensor', values);
  }
}

const getIdSocketUser = (id_user) => {
  const res = Object.entries(connectedUsers).find(([key, value]) => value.id === id_user)
  if (res) {
    return res[0];
  }
  return false
}

module.exports = {
  socket,
  setDataSensor
}