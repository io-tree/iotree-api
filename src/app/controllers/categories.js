const {
  sequelize,
  categories,
  plants
} = require('../models');

const {
  validate,
  yup
} = require('../helpers/validations');

const getCategory = async (req, res, next) => {

  let categories_all = await categories.findAll();

  req.payload = {
    status: 'ok',
    body: categories_all,
  }

  next();

}

const createCategory = async (req, res, next) => {

  const schemaCreateCategory = yup.object().shape({
    name: yup.string().required(),
    description: yup.string(),
    num_order: yup.number().required().notOneOf(req.categories_order)
  });

  const errors = validate(schemaCreateCategory, req.body);

  if (!errors.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: errors.body
    }
    return next();
  }

  let category = (await categories.create(errors.body));

  req.payload = {
    status: 'ok',
    body: category
  }

  next();
}

const updateCategory = async (req, res, next) => {
  const schemaUpdateCategory = yup.object().shape({
    id_category: yup.number().required(),
    name: yup.string()
  })

  const errors = validate(schemaUpdateCategory, {
    id_category: req.params.idCategory,
    ...req.body
  })
  console.log(errors)
  if (!errors.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: errors.body
    }
    return next();
  }

  const {
    id_category,
    name
  } = errors.body

  const category = await categories.findOne({
    where: {
      id_category
    }
  })
  console.log(errors.body)
  if (category) {
    if (name) {
      category.name = name
    }

    await category.save()

    req.payload = {
      status: 'ok'
    }
  } else {
    req.payload = {
      status: 'error',
      msg: 'Não foi possivel alterar categoria'
    }
  }

  next()
}

const deleteCategory = async (req, res, next) => {
  const schemaDeleteCategory = yup.object().shape({
    id_category: yup.number().required().moreThan(1)
  });

  const errors = validate(schemaDeleteCategory, { id_category: req.params.idCategory });

  if (!errors.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: errors.body
    }
    return next();
  }

  const t = await sequelize.transaction();

  let id_category = errors.body.id_category;

  try {
    const category = await categories.findOne({
      where: {
        id_category
      },
      transaction: t
    });

    let pp = await plants.update({
      id_category: 1
    }, {
      where: {
        id_category
      },
      transaction: t
    });

    console.log(pp)

    await category.destroy({
      transaction: t
    });

    await t.commit();

    req.payload = {
      status: 'ok',
    }
  } catch (err) {
    console.log(err)
    await t.rollback();

    req.payload = {
      status: 'error',
    }
  }


  next();
}

module.exports = {
  getCategory,
  createCategory,
  deleteCategory,
  updateCategory
}