const { sendEmail } = require('./emails');

const sendVerifyNewEmail = async (email, name, data) => {
	let nameUser = name;
	let title = 'Verificação de novo email';
	let message = 'verifique seu email para efetuar a mudança, então <b>clique no botão abaixo!</b>';
	let buttonName = 'Verificar novo email';

	return await sendEmail(nameUser, email, title, message, buttonName, '/verification/', data);
}

const sendVerifyEmail = async (email, name, data) => {
	let nameUser = name;
	let title = 'Verificação de email';
	let message = 'verifique seu email para acessar seu app IoTree, então <b>clique no botão abaixo!</b>';
	let buttonName = 'Verificar email';

	return await sendEmail(nameUser, email, title, message, buttonName, '/verification/', data);
}

const sendRecoveryPassword = async (email, name, data) => {
	let nameUser = name;
	let title = 'Redefinição de senha';
	let message = 'Para redefinir a sua senha, <b>clique no botão abaixo</b>.';
	let buttonName = 'Redefinir senha';

	return await sendEmail(nameUser, email, title, message, buttonName, '/password/recovery/', data, { name });
}

module.exports = {
	sendVerifyEmail,
	sendVerifyNewEmail,
	sendRecoveryPassword
}