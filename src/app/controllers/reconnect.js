const {
  yup,
  validate
} = require('../helpers/validations');

const {
  generateToken
} = require('../helpers/token');

const {
  decrypt
} = require('../helpers/crypt');

const {
  users
} = require('../models');

const reconnect = async (req, res, next) => {

  const schema = yup.object().shape({
    keyConnection: yup.string().required()
  });

  const errors = validate(schema, req.body);

  if (!errors.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: errors.body
    }

    return next();
  }

  const {
    keyConnection
  } = errors.body;

  try {

    const {
      email,
      password
    } = JSON.parse(decrypt(keyConnection));

    const user = await users.findOne({
      where: {
        email
      }
    });

    if (user) {
      req.token = generateToken({
        id: user.id_user
      });

      req.refreshToken = generateToken({
        id: user.id_user,
        token: req.token
      }, true);

      req.payload = {
        status: 'ok',
        msg: 'userLogin',
        body: user,
        keyConnection
      }
    } else {
      req.payload = {
        status: 'error',
        msg: 'userNotExist',
      }
    }
  } catch (err) {
    req.payload = {
      status: 'error',
      msg: 'keyConnectionInvalid'
    }
  }


  return next();
}

module.exports = {
  reconnect
}