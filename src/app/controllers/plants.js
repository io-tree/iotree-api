const { plants, categories } = require('../models');
const {
  validate,
  yup
} = require('../helpers/validations');

const getPlant = async (req, res, next) => {

  let pagination = {}
  let where = {}

  if (req.query.page) {
    pagination = {
      limit: 5,
      offset: 5 * (req.query.page - 1)
    }
  }

  if (req.query.plant) where.id_plant = req.query.plant;
  if (req.query.category) where.id_category = req.query.category;

  let plant = await plants.findAll({
    include: [{
      model: categories,
      as: 'category',
      attributes: ['name']
    }],
    where,
    ...pagination,
  });

  req.payload = {
    status: 'ok',
    body: plant,
  }

  next();

}

const createPlant = async (req, res, next) => {

  const schemaCreatePlant = yup.object().shape({
    name: yup.string().required(),
    scientific_name: yup.string().required(),
    description: yup.string(),
    humidity_air: yup.object().shape({
      min: yup.number().required(),
      max: yup.number().required(),
      unid: yup.string().default('%')
    }),
    humidity_ground: yup.object().shape({
      min: yup.number().required(),
      max: yup.number().required(),
      unid: yup.string().default('%')
    }),
    temperature: yup.object().shape({
      min: yup.number().required(),
      max: yup.number().required(),
      unid: yup.string().default('°C')
    }),
    pH: yup.object().shape({
      min: yup.number().required(),
      max: yup.number().required(),
      unid: yup.string().default('pH')
    }),
    height: yup.object().shape({
      min: yup.number().required(),
      max: yup.number().required(),
      unid: yup.string().default('cm')
    }),
    cycle: yup.object().shape({
      min: yup.number().required(),
      max: yup.number().required(),
      unid: yup.string().default('d')
    }),
    id_category: yup.number().required().oneOf(req.categories)
  });

  let errors = validate(schemaCreatePlant, req.body);

  if (!errors.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: errors.body
    }
    return next();
  }

  //errors.name = errors.name.charAt(0).toUpperCase() + errors.name.slice(1);

  let plant = (await plants.create(errors.body));

  req.payload = {
    status: 'ok',
    body: plant
  }

  next();
}

const deletePlant = async (req, res, next) => {
  const plant = await plants.findOne({
    where: {
      id_plant: req.params.idPlant
    }
  });

  if (plant) {
    await plant.destroy();

    req.payload = {
      status: 'ok',
      msg: 'plantDeletedSuccess'
    }

  } else {
    req.payload = {
      status: 'error',
      msg: 'plantNotExist'
    }
  }

  next();
}

module.exports = {
  getPlant,
  createPlant,
  deletePlant
}