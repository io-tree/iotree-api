const {
	validations,
	removeKeys,
} = require('../helpers');
const {
	posts,
	comments,
	users,
	post_reacts
} = require('../models');

const getPost = async (req, res, next) => {

	let pagination = {}
	let where = {}

	if (req.query.page) {
		pagination = {
			limit: 5,
			offset: 5 * (req.query.page - 1)
		}
	}

	if (req.query.user) where.id_user = req.query.user;
	if (req.query.post) where.id_post = req.query.post;

	let all_post = await posts.findAll({
		include: [{
			model: comments,
			as: 'comments',
			include: [{
				model: users,
				as: 'users',
				attributes: ['id_user', 'name', 'path_img']
			}]
		}, {
			model: users,
			as: 'users',
			attributes: ['id_user', 'name', 'path_img']
		}, {
			model: post_reacts,
			as: 'reacts'
		}],
		order: [['createdAt', 'DESC'], [comments, 'createdAt', 'DESC']],
		where,
		...pagination,
	});

	all_post = JSON.parse(JSON.stringify(all_post))

	all_post.map(elem => {
		elem.path_img = elem.path_img.split(',')
	})

	req.payload = {
		status: 'ok',
		body: {
			count: all_post.length,
			rows: all_post
		}
	}

	next();
}

const createPost = async (req, res, next) => {
	const {
		title,
		description
	} = req.body;
	//TODO: melhorar rota
	let post = (await posts.create({
		id_user: req.user.id_user,
		title,
		description,
		path_img: ''
	})).get({
		plain: true
	});

	post = removeKeys(post, [
		'id_user',
		'updatedAt',
		'createdAt'
	])

	req.payload = {
		status: 'ok',
		msg: 'postCreatedSuccess',
		body: post
	}

	next();

}

const deletePost = async (req, res, next) => {
	let post = await posts.findOne({
		where: {
			id_post: req.params.idPost
		}
	});

	if (post) {
		await post.destroy();
		req.payload = {
			status: 'ok',
			msg: 'postDeletedSuccess'
		}

	} else {
		req.payload = {
			status: 'error',
			msg: 'postNotExist'
		}
	}
	next();
}

const updatePost = async (req, res, next) => {

	const {
		title,
		description
	} = req.body;

	let post = await posts.findOne({
		where: {
			id_post: req.params.idPost
		}
	});

	if (post) {
		post.title = title;
		post.description = description;

		await post.save();

		req.payload = {
			status: 'ok',
			msg: 'postUpdatedSuccess',
			body: post.toString()
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'postNotExist'
		}
	}

	next();
}

module.exports = {
	createPost,
	getPost,
	deletePost,
	updatePost
}