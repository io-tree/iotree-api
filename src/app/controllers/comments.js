const {
	comments,
	posts
} = require('../models');
const {
	getFieldText,
	getMessageText,
	sendResponse
} = require('../middleware/result');

const {
	validations,
} = require('../helpers');

const getComment = async (req, res, next) => {

	let where = {}
	let pagination = {}

	if (req.query.page) {
		pagination = {
			limit: 5,
			offset: 5 * (req.query.page - 1)
		}
	}

	if (req.query.comment) where.id_comment = req.query.comment;

	const all_comment = await comments.findAndCountAll({
		where,
		...pagination,
	})


	req.payload = {
		status: 'ok',
		body: all_comment
	}

	next();
}

const createComment = async (req, res, next) => {
	const {
		text,
		in_comment
	} = req.body;
	//TODO: muda a validação para yup
	let valid = new validations();

	valid.isExist(text, getFieldText().comment);

	if (valid.hasError()) {
		req.payload = {
			status: 'error',
			msg: valid.getError(),
		};
		return next();
	}

	const postId = req.params.idPost;

	const post = await posts.findOne({
		where: {
			id_post: postId
		}
	});

	if (post) {
		let comment = (await comments.create({
			text,
			id_user: req.user.id_user,
			id_post: postId,
			in_comment
		})).get({
			plain: true
		});

		req.payload = {
			status: 'ok',
			msg: 'commentCreatedSuccess',
			body: comment
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'postNotExist',
		}
	}



	next();
}

const updateComment = async (req, res, next) => {
	const {
		text
	} = req.body;
	//TODO: muda a validação para yup
	let valid = new validations();

	valid.isExist(text, getFieldText().comment);

	if (valid.hasError()) {
		req.payload = {
			status: 'error',
			msg: valid.getError(),
		};
		return next();
	}

	let comment = await comments.findOne({
		where: {
			id_comment: req.params.idComment
		}
	});

	const post = await posts.findOne({
		where: {
			id_post: req.params.idPost
		}
	});

	if (comment && post) {
		comment.text = text;
		await comment.save();

		req.payload = {
			status: 'ok',
			msg: 'commentUpdatedSuccess',
			body: comment
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'commentNotExist'
		}
	}

	next();
}

const deleteComment = async (req, res, next) => {
	let comment = await comments.findOne({
		where: {
			id_comment: req.params.idComment
		}
	});

	const post = await posts.findOne({
		where: {
			id_post: req.params.idPost
		}
	});

	if (comment && post) {
		await comment.destroy();

		req.payload = {
			status: 'ok',
			msg: 'commentDeletedSuccess'
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'commentNotExist'
		}
	}

	next();


}

module.exports = {
	getComment,
	createComment,
	updateComment,
	deleteComment,
}