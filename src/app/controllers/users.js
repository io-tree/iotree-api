const moment = require('moment');
const { yup, validate } = require('../helpers/validations');

const {
	removeKeys,
	crypt
} = require('../helpers');
const {
	users,
	genders
} = require('../models');

const {
	sendVerifyEmail,
	sendVerifyNewEmail
} = require('../controllers/sendEmail');

const showUser = async (req, res, next) => {
	req.payload = {
		status: 'ok',
		body: req.user
	}

	return next();
};

const createUser = async (req, res, next) => {
	let schemaCreateUser = yup.object().shape({
		name: yup.string().min(3).required(),
		birthday_date: yup.date().required(),
		email: yup.string().email().required(),
		password: yup.string().min(6).required(),
		id_gender: yup.number().required().oneOf(req.genders)
	});

	let errors = validate(schemaCreateUser, req.body);

	if (!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}

	let {
		name,
		birthday_date,
		email,
	} = errors.body;

	let user = await users.findOne({
		raw: true,
		where: {
			email: email
		}
	});

	if (!user) {
		req.body.birthday_date = new Date(birthday_date);

		user = (await users.create(req.body)).get({
			plain: true
		});

		user = removeKeys(user, [
			'password',
			'createdAt',
			'updatedAt',
		]);

		req.user = user;

		let data = {
			id: user.id_user,
			type: 'checker_email'
		};

		if(await sendVerifyEmail(email, name, data)){
			req.payload = {
				status: 'ok',
				msg: 'userCreatedSuccess',
				body: user
			}
		} else {
			req.payload = {
				status: 'error',
				msg: 'sendEmailError',
			}
		}


	} else {
		req.payload = {
			status: 'error',
			msg: 'userExist',
		}
	}

	next();
};

const updateUser = async (req, res, next) => {

	let schemaUserUpdate = yup.object().shape({
		name: yup.string().min(3).max(80),
		birthday_date: yup.date(),
		email: yup.string().email(),
		password: yup.string().min(6).required(),
		password_new: yup.string().min(6),
		id_gender: yup.number()
	});

	let erros = validate(schemaUserUpdate, req.body);

	if (!erros.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: erros.body
		}

		return next();
	}

	let {
		password,
		password_new,
		email,
		id_gender,
		name,
		birthday_date,
	} = erros.body;

	let user = await users.findOne({
		where: {
			id_user: req.user.id_user
		}
	})

	if (crypt.compareHash(password, user.password)) {

		if (password_new) {
			user.password = crypt.createHash(password_new);
		}

		if (email && email !== user.email) {
			let data = {
				id: user.id_user,
				email,
				type: 'changer_email'
			}
			//TODO: montar verificação de memail existente
			if(! await sendVerifyNewEmail(email, name, data)){
				req.payload = {
					status: 'error',
					msg: 'sendEmailError'
				}

				return next()
			}
		}

		if (name) {
			user.name = name;
		}

		if (id_gender) {
			user.id_gender = id_gender
		}

		if (birthday_date) {
			user.birthday_date = moment(new Date(birthday_date))
		}

		try {
			await user.save();

			req.user = user;
			user = removeKeys(user, [
				'password',
				'createdAt',
				'updatedAt'
			]);

			if (email && email !== user.email) {
				req.payload = {
					status: 'ok',
					msg: 'userUpdatedSuccessAndEmailSend',
					body: user
				}
			} else {
				req.payload = {
					status: 'ok',
					msg: 'userUpdatedSuccess',
					body: user
				}
			}

		} catch (err) {
			if (err.name == 'SequelizeUniqueConstraintError') {
				req.payload = {
					status: 'error',
					msg: 'emailAlreadyExist'
				}
			}
		}

	} else {
		req.payload = {
			status: 'ok',
			msg: 'passwordWrong'
		}
	}

	next();
};

const setUserAdmin = async (req, res, next) => {
	const schemaSetUserAdmin = yup.object().shape({
		id_user: yup.string().required(),
		is_admin: yup.boolean().required()
	});

	const errors = validate(schemaSetUserAdmin, req.body);


	if (!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}

	const {
		id_user,
		is_admin
	} = errors.body;

	const admin = await users.findOne({
		where: {
			is_admin: true
		},
		order: [ [ 'createdAt', 'ASC' ]]
	});

	if(admin.id_user === id_user) {
		req.payload = {
			status: 'error',
			msg: 'cantDeleteAdmin'
		}

		return next();
	}

	const user = await users.findOne({
		where: {
			id_user,
			is_enabled: true
		}
	});

	if (user) {
		user.is_admin = is_admin;
		await user.save();

		req.payload = {
			status: 'ok',
			msg: 'userUpdatedSuccess',
			body: user
		}

	} else {
		req.payload = {
			status: 'error',
			msg: 'userNotExist'
		}
	}


	next();

}

const getAllUser = async (req, res, next) => {
	let pagination = {}
	let where = {}

	if (req.query.page) {
		pagination = {
			limit: 10,
			offset: 10 * (req.query.page - 1)
		}
	}

	if (req.query.user) where.id_user = req.query.user;

	let all_users = await users.findAll({
		include: [{
			model: genders,
			as: 'gender',
			attributes: ['name']
		}],
		//order: [['createdAt', 'DESC']],
		where,
		...pagination,
	});

	all_users = JSON.parse(JSON.stringify(all_users))

	req.payload = {
		status: 'ok',
		body: {
			count: all_users.length,
			rows: all_users
		}
	}

	next();
}

const deleteUser = async (req, res, next) => {
	const count = await users.findAll({
		where: {
			is_admin: true
		}
	})

	const user = await users.findOne({
		where: {
			id_user: req.user.id_user
		}
	});

	if (user) {
		if (!req.user.is_admin || (req.user.id_admin && count.length > 1)) {
			await user.destroy();
			req.payload = {
				status: 'ok',
				msg: 'userDeletedSuccess'
			}
		} else {
			req.payload = {
				status: 'error',
				msg: 'userDeleteError'
			}
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'userDeleteError'
		}
	}

	next();
};

module.exports = {
	showUser,
	createUser,
	updateUser,
	deleteUser,
	setUserAdmin,
	getAllUser
}