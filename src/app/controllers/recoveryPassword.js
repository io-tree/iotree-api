const yup = require('yup');
const moment = require('moment');

const {
	users
} = require('../models');

const {
	sendRecoveryPassword
} = require('./sendEmail');

const { decrypt, createHash } = require('../helpers/crypt');

const sendRequest = async (req, res, next) => {
	const {
		email
	} = req.body;

	const schema = yup.string().email().required().isValidSync(email);

	if (schema) {
		const user = await users.findOne({
			where: {
				email
			}
		});

		if (user) {

			if (user.passwordChangedAt && moment(new Date()).diff(new Date(user.passwordChangedAt), 'minutes') < 60) {
				req.payload = {
					status: 'ok',
					msg: 'passwordWaitTime',
					token: false
				}
			} else {

				let data = {
					id: user.id_user,
					date: new Date()
				}

				if( await sendRecoveryPassword(user.email, user.name, data)){
					req.payload = {
						status: 'ok',
						msg: 'emailRecoveryPasswordSuccess'
					}
				} else {
					req.payload = {
						status: 'error',
						msg: 'sendEmailError'
					}
				}

			}


		} else {
			req.payload = {
				status: 'error',
				msg: 'emailNotFound'
			}
		}



	} else {
		req.payload = {
			status: 'error',
			msg: 'emailInvalid'
		}
	}


	next();

}

const changerPassword = async (req, res, next) => {
	let {
		hash,
		password
	} = req.body;
	//TODO: melhorar validação yup
	const schema = yup.object().shape({
		password: yup.string().required().nullable(false),
		hash: yup.string().required()
	});

	if (schema.isValidSync(req.body)) {
		const { id, date } = JSON.parse(decrypt(hash, true));

		let minutes = moment(new Date()).diff(new Date(date), "minutes");

		if (minutes > 10) {
			req.payload = {
				status: 'ok',
				msg: 'passwordRecoveryTimeOver'
			}
		} else {
			const user = await users.findOne({
				where: {
					id_user: id
				}
			});

			if (user.passwordChangedAt && moment(new Date()).diff(new Date(user.passwordChangedAt), 'minutes') < 60) {
				req.payload = {
					status: 'ok',
					msg: 'passwordWaitTime'
				}
			} else {

				user.password = createHash(password);
				user.passwordChangedAt = new Date();

				await user.save();

				req.payload = {
					status: 'ok',
					msg: 'passwordRecoverySuccess'
				}
			}

		}

	} else {
		req.payload = {
			status: 'error',
			msg: 'emailHashInvalid'
		}
	}

	next();
}


module.exports = {
	sendRequest,
	changerPassword
}