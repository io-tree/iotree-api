const { yup, validate } = require('../helpers/validations');
const moment = require('moment');
const {
	removeKeys,
	crypt,
	token
} = require('../helpers');
const {
	users,
	plantations,
	genders
} = require('../models');


const loginUser = async (req, res, next) => {

	const schemaLoginUser = yup.object().shape({
		email: yup.string().email().required(),
		password: yup.string().min(6).required(),
		stayConnected: yup.boolean().required()
	});

	let erros = validate(schemaLoginUser, req.body);

	if (!erros.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: erros.body
		}

		return next();
	}

	const {
		email,
		password,
		stayConnected
	} = erros.body;

	let user = await users.findOne({
		where: {
			email: email
		},
		include: [{
			model: plantations,
			as: 'plantations'
		}, {
			model: genders,
			as: 'gender',
			attributes: ['name']
		}],
	});

	if (user) {
		if (!user.is_enabled) {
			req.payload = {
				status: 'error',
				msg: 'userVerifyEmail'
			}
			return next();
		}
		if (user.blockedAt) {
			let time = moment(new Date()).diff(new Date(user.blockedAt), 'minutes');
			if (time > 30) {
				user.blockedAt = null;
				await user.save();
			} else {
				req.payload = {
					status: 'error',
					msg: 'userBlocked'
				}
				return next();
			}
		}

		if (crypt.compareHash(password, user.password)) {
			user.login_attempts = 0;
			await user.save();

			req.token = token.generateToken({
				id: user.id_user
			});

			req.refreshToken = token.generateToken({
				id: user.id_user,
				token: req.token
			}, true);

			let keyConnection = undefined;

			if (stayConnected) {
				keyConnection = crypt.encrypt(JSON.stringify({
					email,
					password
				}));
			}

			req.payload = {
				status: 'ok',
				msg: 'userLogin',
				body: user,
				keyConnection
			}

		} else {

			user.login_attempts = user.login_attempts + 1;
			await user.save();
			if (user.login_attempts > 2) {

				user.login_attempts = 0;
				user.blockedAt = new Date();
				await user.save();

				req.payload = {
					status: 'ok',
					msg: 'userBlocked'
				}
			} else {
				req.payload = {
					status: 'error',
					msg: 'loginFailed'
				}
			}
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'userNotExist'
		}
	}

	next();
}

const loginGreenhouse = async (req, res, next) => {
	const id_greenhouse = req.params.idGreenhouse;

	const plantation = await plantations.findOne({
		where: {
			id_greenhouse
		}
	});

	req.payload = {
		status: 'ok',
		body: plantation
	}


	next();
}

module.exports = {
	loginUser,
	loginGreenhouse
}