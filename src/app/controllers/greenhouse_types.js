const {
	yup,
	validate
} = require('../helpers/validations');

const {
	sequelize,
	greenhouse_types,
	greenhouses
} = require('../models');

const getGreenhouseType = async (req, res, next) => {
	const greenhouse_type = await greenhouse_types.findAll({
		order: [['num_order', 'asc']]
	});

	req.payload = {
		status: 'ok',
		body: greenhouse_type
	}

	next();
}

const createGreenhouseType = async (req, res, next) => {

	const schemaCreateGreenhouseType = yup.object().shape({
		name: yup.string().min(3).required(),
		initials: yup.string().length(3).required().notOneOf(req.greenhouse_types_initials),
		num_order: yup.number().positive().required().notOneOf(req.greenhouse_types_order)
	});

	let errors = validate(schemaCreateGreenhouseType, req.body);

	if (!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}

	const greenhouse_type = await greenhouse_types.create(errors.body);

	req.payload = {
		status: 'ok',
		msg: 'greenhouseTypeCreatedSuccess',
		body: greenhouse_type
	}

	next();
}
const updateGender = (req, res, next) => {

}

const deleteGreenhouseType = async (req, res, next) => {
	const schemaDeleteGreenhouseType = yup.object().shape({
		id_greenhouse_type: yup.number().required().moreThan(1)
	});

	const errors = validate(schemaDeleteGreenhouseType, { id_greenhouse_type: req.params.idGreenhouseType });

	if (!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}
		return next();
	}

	const t = await sequelize.transaction();

	let id_greenhouse_type = errors.body.id_greenhouse_type;

	try {
		const greenhouse_type = await greenhouse_types.findOne({
			where: {
				id_greenhouse_type
			},
			transaction: t
		});

		await greenhouses.update({
			id_greenhouse_type: 1
		}, {
			where: {
				id_greenhouse_type
			},
			transaction: t
		});

		await greenhouse_type.destroy({
			transaction: t
		});

		await t.commit();

		req.payload = {
			status: 'ok',
			msg: 'genderDeletedSuccess',
		}
	} catch (err) {
		console.log(err)
		await t.rollback();

		req.payload = {
			status: 'error',
			msg: 'genderDeletedError',
		}
	}


	next();

}


module.exports = {
	getGreenhouseType,
	createGreenhouseType,
	deleteGreenhouseType
}