const {
	yup,
	validate
} = require('../helpers/validations');

const {
	sequelize,
	genders,
	users
} = require('../models');

const getGender = async (req, res, next) => {
	const gender = await genders.findAll({
		order: [['num_order', 'asc']]
	});

	req.payload = {
		status: 'ok',
		body: gender
	}

	next();
}

const createGender = async (req, res, next) => {

	const schemaCreateGender = yup.object().shape({
		name: yup.string().min(3).required(),
		num_order: yup.number().positive().required().notOneOf(req.genders_order)
	});

	let errors = validate(schemaCreateGender, req.body);

	if (!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}

	const gender = await genders.create(errors.body);

	req.payload = {
		status: 'ok',
		msg: 'genderCreatedSuccess',
		body: gender
	}

	next();
}
const updateGender = (req, res, next) => {

}

const deleteGender = async (req, res, next) => {
	const schemaDeleteGender = yup.object().shape({
		id_gender: yup.number().required().moreThan(1)
	});

	const errors = validate(schemaDeleteGender, { id_gender: req.params.idGender });

	if (!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}
		return next();
	}

	const t = await sequelize.transaction();

	let id_gender = errors.body.id_gender;

	try {
		const gender = await genders.findOne({
			where: {
				id_gender
			},
			transaction: t
		});

		await users.update({
			id_gender: 1
		}, {
			where: {
				id_gender
			},
			transaction: t
		});

		await gender.destroy({
			transaction: t
		});

		await t.commit();

		req.payload = {
			status: 'ok',
			msg: 'genderDeletedSuccess',
		}
	} catch (err) {
		console.log(err)
		await t.rollback();

		req.payload = {
			status: 'error',
			msg: 'genderDeletedError',
		}
	}


	next();

}


module.exports = {
	getGender,
	createGender,
	deleteGender
}