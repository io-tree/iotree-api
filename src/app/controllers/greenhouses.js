const faker = require('faker');
const {
	greenhouses,
	greenhouse_types,
	plantations,
	Op
} = require('../models');
const {
	yup,
	validate
} = require('../helpers/validations');
const {
	compareHash,
	createHash
} = require('../helpers/crypt');

const createGreenhouse = async (req, res, next) => {

	const schemaCreateGreenhouse = yup.object().shape({
		code: yup.string().default(''),
		password: yup.string().default('000000'),
		id_type: yup.string().required().oneOf(req.greenhouse_types),
		name: yup.string().default(faker.address.city())
	});

	const errors = validate(schemaCreateGreenhouse, req.body);

	if(!errors.isValid) {
		req.payload= {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}

	let greenhouse = await greenhouses.create(errors.body);

	req.payload = {
		status: 'ok',
		body: greenhouse
	}

	next();
}

const getAllGreenhouse = async (req, res, next) => {
	let greenhouse = await greenhouses.findAll({
		include: [
			{
				model: greenhouse_types,
				as: 'greenhouse_type'
			},
			{
				model: plantations,
				as: 'plantation'
			}
		]
	});

	req.payload = {
		status: 'ok',
		body: greenhouse
	}

	next();
}

const getGreenhouse = async (req, res, next) => {

	let where = {
		id_user: req.user.id_user
	}

	if(req.params.idGreenhouse){
		where.id_greenhouse = req.params.idGreenhouse
	}

	let greenhouse = await greenhouses.findAll({
		where,
		include: [
			{
				model: greenhouse_types,
				as: 'greenhouse_type'
			},
			{
				model: plantations,
				as: 'plantation'
			}
		]
	});

	req.payload = {
		status: 'ok',
		body: greenhouse
	}

	next();
}

const updateGreenhouse = async (req, res, next) => {
	const schemaUpdateGreenhouse = yup.object().shape({
		name: yup.string().min(2),
		password: yup.string().min(6),
		new_password: yup.string().min(6),
		confirm_password: yup.boolean().required()
	});

	const errors = validate(schemaUpdateGreenhouse, req.body);

	if(!errors.isValid){
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}

	const greenhouse = await greenhouses.findOne({
		where: {
			id_greenhouse: req.params.idGreenhouse,
			id_user: req.user.id_user
		}
	});

	const {
		name,
		password,
		new_password,
		confirm_password
	} = errors.body;


	if(greenhouse){

		if(confirm_password){
			if(!compareHash(password, greenhouse.password)){
				req.payload = {
					status: 'error',
					msg: 'passwordWrong'
				}

				return next()
			}
		}

		if(name) {
			greenhouse.name = name;
		}

		if(new_password) {
			greenhouse.password = createHash(new_password);
		}

		await greenhouse.save();

		req.payload = {
			status: 'ok',
			msg: 'greenhouseUpdatedSuccess',
			body: greenhouse
		}

	} else {
		req.payload = {
			status: 'error',
			msg: 'greenhouseNotExist'
		}
	}


	return next();

}

const setLinkGreenhouse = async (req, res, next) => {

	const json = {
		id_greenhouse: req.params.idGreenhouse,
		password: req.body.password,
		link: req.body.link
	};

	const schemaLinkGreenhouse = yup.object().shape({
		id_greenhouse: yup.string().required(),
		password: yup.string().min(6).required(),
		link: yup.boolean().required()
	});

	const errors = validate(schemaLinkGreenhouse, json);

	if(!errors.isValid) {
		req.payload = {
			status: 'error',
			msg: 'validationError',
			body: errors.body
		}

		return next();
	}
	const {
		id_greenhouse,
		password,
		link
	} = errors.body;

	const greenhouse = await greenhouses.findOne({
		where: {
			[Op.or]: [
				{ id_greenhouse },
				{ code: id_greenhouse}
			]
		}
	});

	if(compareHash(password, greenhouse.password)) {
		if(greenhouse) {
			if(greenhouse.id_user && link){
				req.payload = {
					status: 'error',
					msg: (greenhouse.id_user === req.user.id_user ? "greenhouseAlreadyLinkedYourAccount" : "greenhouseAlreadyLinked")
				}
			} else {
				greenhouse.id_user = (link ? req.user.id_user : null);
				await greenhouse.save();
				req.payload = {
					status: 'ok',
					msg: (link ? 'greenhouseLinkedSuccess' : 'greenhouseUnlinkedSuccess')
				}
			}
		} else {
			req.payload = {
				status: 'ok',
				msg: 'greenhouseNotExist'
			}
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'passwordWrong'
		}
	}


	return next();
}

module.exports = {
	createGreenhouse,
	getGreenhouse,
	getAllGreenhouse,
	updateGreenhouse,
	setLinkGreenhouse
}