const moment = require('moment');
const { removeKeys, token, crypt } = require('../helpers');
const {
  yup,
  validate
} = require('../helpers/validations');
const {
  plantations,
  greenhouses,
  plants,
  Op
} = require('../models');

const { setDataSensor } = require('./socket');

const getPlantation = async (req, res, next) => {

  let where = {};

  if (req.params.idPlantation) {
    where.id_plantation = req.params.idPlantation;
  }

  const plantation = await plantations.findAll({
    include: [
      {
        model: plants,
        as: 'plants'
      },
      {
        model: greenhouses,
        as: 'greenhouses'
      }
    ],
    where: {
      id_user: req.user.id_user,
      ...where
    }
  });

  req.payload = {
    status: 'ok',
    body: plantation
  }

  next();
}

const createPlantation = async (req, res, next) => {

  const schemaCreatePlantation = yup.object().shape({
    id_greenhouse: yup.string().required(),
    id_plant: yup.string().required()
  });

  const erros = validate(schemaCreatePlantation, req.body);

  if (!erros.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: erros.body
    }

    return next();
  }

  const {
    id_greenhouse,
    id_plant
  } = erros.body;

  let greenhouse = await greenhouses.findOne({
    where: {
      [Op.or]: [
        { id_greenhouse: id_greenhouse },
        { code: id_greenhouse }
      ]
    }
  });

  let plant = await plants.findOne({
    where: {
      id_plant: id_plant
    }
  });

  let plantation = await plantations.findOne({
    where: {
      id_greenhouse,
      is_done: false
    }
  });

  if (plantation) {
    req.payload = {
      status: 'error',
      msg: 'greenhouseAlreadyUsed'
    }
  } else if (!greenhouse) {
    req.payload = {
      status: 'error',
      msg: 'greenhouseNotExist'
    }
  } else if (!plant) {
    req.payload = {
      status: 'error',
      msg: 'plantNotExist'
    }
  } else {
    console.log(plant.toJSON().temperature)
    plant = plant.toJSON();
    //console.log(moment().add(plant.cycle.max, 'days'))
    console.log(plant.cycle.max)
    //console.log(new Date().setDate(new Date().getDate() + ))
    let plantation = (await plantations.create({
      id_plant,
      id_greenhouse: greenhouse.id_greenhouse,
      id_user: req.user.id_user,
      temperature: (plant.temperature.max - plant.temperature.min).toString(),
      humidity_air: (plant.humidity_air.max - plant.humidity_air.min).toString(),
      humidity_ground: (plant.humidity_ground.max - plant.humidity_ground.min).toString(),
      time_irrigation: moment(new Date()),
      done_time: moment().add(plant.cycle.max, 'days')
    })).get({ plain: true });

    plantation = removeKeys(plantation, [
      'createdAt',
      'updatedAt',
    ]);

    req.payload = {
      status: 'ok',
      msg: 'plantationCreatedSuccess',
      body: plantation
    }
  }


  next();
}

const deletePlantation = async (req, res, next) => {
  const schemaDeletePlantation = yup.object().shape({
    id_plantation: yup.string().required()
  });

  const erros = validate(schemaDeletePlantation, { id_plantation: req.params.idPlantation });

  if (!erros.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: erros.body
    }

    return next();
  }

  const {
    id_plantation
  } = erros.body;

  const plantation = await plantations.findOne({
    where: {
      id_user: req.user.id_user,
      id_plantation: id_plantation
    }
  });

  if (plantation) {
    await plantation.destroy();

    req.payload = {
      status: 'ok',
      msg: 'plantationDeletedSuccess'
    }
  } else {
    req.payload = {
      status: 'error',
      msg: 'plantationNotExist'
    }
  }

  return next();
}


const updatePlantation = async (req, res, next) => {
  const schemaPlantationUpdate = yup.object().shape({
    temperature: yup.number().required(),
    humidity_air: yup.number().required(),
    humidity_ground: yup.number().required(),
  });
  console.log(req.body)
  const erros = validate(schemaPlantationUpdate, { id_plantation: req.params.idPlantation, ...req.body });

  if (!erros.isValid) {
    req.payload = {
      status: 'error',
      msg: 'validationError',
      body: erros.body
    }

    return next();
  }

  const {
    id_plantation,
    temperature,
    humidity_air,
    humidity_ground
  } = erros.body;

  const plantation = await plantations.findOne({
    where: {
      id_plantation
    }
  });

  plantation.temperature = temperature.toString();
  plantation.humidity_air = humidity_air.toString();
  plantation.humidity_ground = humidity_ground.toString();

  const time = moment(new Date()).diff(new Date(plantation.time_irrigation))
  console.log(time)
  if (time > 0) {
    plantation.time_irrigation = moment(new Date()).add(3, 'h')
  }

  await plantation.save();

  req.payload = {
    status: 'ok'
  }

  setDataSensor(plantation.id_user, erros.body)

  next();
}

module.exports = {
  getPlantation,
  createPlantation,
  updatePlantation,
  deletePlantation
}