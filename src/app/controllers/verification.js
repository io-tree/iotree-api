const yup = require('yup');

const {
	users
} = require('../models');

const {
	crypt
} = require('../helpers');

const {
	sendVerifyEmail
} = require('./sendEmail');

const verificationEmail = async (req, res, next) => {
	const hash = req.params.hash;

	const schemaChecker = yup.object().shape({
		hash: yup.string().required()
	})

	try {
		schemaChecker.validateSync({
			hash
		});
	} catch (err) {
		console.log(err)
	}

	const data = JSON.parse(crypt.decrypt(hash, true));

	if (data.type === 'checker_email') {

		if (data.id) {

			let user = await users.findOne({
				where: {
					id_user: data.id,
					is_enabled: false
				}
			});
			if (user) {
				user.is_enabled = true;
				await user.save();

				req.payload = {
					status: 'ok',
					msg: 'emailVerified'
				}
			} else {
				req.payload = {
					status: 'ok',
					msg: 'emailAlreadyVerified'
				}
			}

		} else {
			req.payload = {
				status: 'error',
				msg: 'emailHashInvalid'
			}
		}
	} else if (data.type === 'changer_email') {
		if (data.id) {
			let user = await users.findOne({
				where: {
					id_user: data.id
				}
			});

			user.email = data.email;
			await user.save();

			req.payload = {
				status: 'ok',
				msg: 'emailVerified'
			}

		} else {
			req.payload = {
				status: 'error',
				msg: 'emailHashInvalid'
			}
		}
	} else {
		req.payload = {
			status: 'error',
			msg: 'validationError'
		}
	}



	return next();
}

const reSendEmail = async (req, res, next) => {
	const user_id = req.params.hash;

	const user = await users.findOne({
		where: {
			email: user_id
		}
	});

	if (user) {
		let hash = {
			id: user.id_user,
			type: 'checker_email'
		};

		if (await sendVerifyEmail(user.email, user.name, hash)) {
			req.payload = {
				status: 'ok',
				msg: 'reSendEmailSuccess'
			}
		} else {
			req.payload = {
				status: 'error',
				msg: 'sendEmailError'
			}
		}

	} else {
		req.payload = {
			status: 'error',
			msg: 'userNotExist'
		}
	}

	next();
}

module.exports = {
	verificationEmail,
	reSendEmail
}