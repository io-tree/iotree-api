const jwt = require('jsonwebtoken');
const secretKeyToken = process.env.SECRET_KEY_TOKEN;
const sessionTime = process.env.SESSION_TIME;
const { encrypt, decrypt } = require('../helpers/crypt');

const generateToken = (value, refresh = false) => {
	value = encrypt(JSON.stringify(value)).toString();
	const hash = jwt.sign({ key: value }, secretKeyToken, {
		algorithm: "HS256",
		expiresIn: 60 * sessionTime * (refresh ? 2 : 1)
	});

	return hash;
};

const verifyToken = (token) => {
	try {
		return JSON.parse(decrypt(jwt.verify(token, secretKeyToken).key));
	} catch (e) {
		console.log(e.message)
		return false
	}
};

module.exports = {
	generateToken,
	verifyToken,
}