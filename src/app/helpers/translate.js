module.exports.languages = [
  {
    name: 'Portugues Brasil',
    code: 'pt-br'
  },
  {
    name: 'English US',
    code: 'en-us'
  }
]

module.exports.message = {
  tokenInvalid: {
    'pt-br': 'Token Inválido.',
    'en-us': 'Invalid Token.'
  },
  tokenNotExist: {
    'pt-br': 'Sem token de acesso.',
    'en-us': 'Within Token access.'
  },
  //============================================================================================
  badRequest: {
    'pt-br': 'Erro no requisição.',
    'en-us': 'request error.'
  },
  validationError: {
    'pt-br': 'Erro na validação.',
    'en-us': 'Validation Error.'
  },
  cantDeleteAdmin: {
    'pt-br': 'Não pode tirar o admin da conta principal.',
    'en-us': 'Can´t remove admin of main account.'
  },
  loginFailed: {
    'pt-br': 'Usuário ou Senha incorreta.',
    'en-us': 'Incorrect User or Password.'
  },
  keyConnectionInvalid: {
    'pt-br': 'Chave de conexão incorreta, faça o login novamente.',
    'en-us': 'Connection key incorrect, do signup again.'
  },
  //============================================================================================
  userCreatedSuccess: {
    'pt-br': 'Usuário criado com sucesso',
    'en-us': 'User created successfully'
  },
  userUpdatedSuccess: {
    'pt-br': 'Usuário atualizado com sucesso.',
    'en-us': 'User updated successfully.'
  },
  userDeletedSuccess: {
    'pt-br': 'Usuário deletado com sucesso.',
    'en-us': 'User deleted successfully.'
  },
  userExist: {
    'pt-br': 'Usuário já existe.',
    'en-us': 'User already exist.'
  },
  userSignInError: {
    'pt-br': 'Erro ao cadastrar usuário.',
    'en-us': 'Error registering user.'
  },
  userUpdateError: {
    'pt-br': 'Erro ao atualizar usuário.',
    'en-us': 'Error updating user.'
  },
  userDeleteError: {
    'pt-br': 'Erro ao deletar usuário.',
    'en-us': 'Error deleting user.'
  },
  userNotExist: {
    'pt-br': 'Usuário não encontrado.',
    'en-us': 'User not found.'
  },
  userBlocked: {
    'pt-br': 'Usuário bloqueado.',
    'en-us': 'User blocked.'
  },
  userLogin: {
    'pt-br': 'Usuário permitido.',
    'en-us': 'User allowed.'
  },
  userVerifyEmail: {
    'pt-br': 'Verifique seu email na sua caixa de mensagens',
    'en-us': 'Verify your email in your inbox'
  },
  userUpdatedSuccessAndEmailSend: {
    'pt-br': 'Usuário atualizado e um email foi enviado para sua novo email para verificação.',
    'en-us': 'User updated successfully and an email was sended to your new email to verification.'
  },
  userNotAuthorization: {
    'pt-br': 'Usuário não é autorizado',
    'en-us': "User isn't authorized"
  },
  //============================================================================================
  passwordWrong: {
    'pt-br': 'Senha incorreta.',
    'en-us': 'Incorrect Password.'
  },
  passwordRecoverySuccess: {
    'pt-br': 'Senha redefinida com sucesso',
    'en-us': 'Password redifined successfully'
  },
  passwordRecoveryTimeOver: {
    'pt-br': 'Redefinição de senha expirado, tente novamente',
    'en-us': 'Redefinition password expired, try again'
  },
  passwordWaitTime: {
    'pt-br': 'Aguarde 1hr para troca de senha novamente',
    'en-us': 'Wait for 1hr to change password again'
  },
  //============================================================================================
  commentNotExist: {
    'pt-br': 'Comentário não existe.',
    'en-us': 'Comment not exist.'
  },
  commentDeletedSuccess: {
    'pt-br': 'Comentário deletado com sucesso.',
    'en-us': 'Comment deleted successfully.'
  },
  commentCreatedSuccess: {
    'pt-br': 'Comentário criado com sucesso.',
    'en-us': 'Comment created successfully.'
  },
  commentUpdatedSuccess: {
    'pt-br': 'Comentário atualizado com sucesso.',
    'en-us': 'Comment created successfully.'
  },
  //============================================================================================
  postNotExist: {
    'pt-br': 'Post não existe.',
    'en-us': 'Post not exist.'
  },
  postDeletedSuccess: {
    'pt-br': 'Post deletado com sucesso.',
    'en-us': 'Post deleted successfully.'
  },
  postCreatedSuccess: {
    'pt-br': 'Post criado com sucesso.',
    'en-us': 'Post created successfully.'
  },
  postUpdatedSuccess: {
    'pt-br': 'Post atualizado com sucesso.',
    'en-us': 'Post created successfully.'
  },
  //============================================================================================
  emailCheckerTitle: {
    'pt-br': 'Verificação de Email',
    'en-us': 'Email Verification'
  },
  emailClosePage: {
    'pt-br': 'Você já pode fechar essa página.',
    'en-us': 'You can close this page.'
  },
  emailVerified: {
    'pt-br': 'Email verificado com sucesso.',
    'en-us': 'Email verified successfully.'
  },
  emailAlreadyVerified: {
    'pt-br': 'Email já verificado com sucesso.',
    'en-us': 'Email already verified successfully.'
  },
  emailChecking: {
    'pt-br': 'Verificando seu email, aguarde...',
    'en-us': 'Checking your email, please wait ...'
  },
  emailHashInvalid: {
    'pt-br': 'Hash é inválido',
    'en-us': 'This hash is invalid'
  },
  reSendEmailSuccess: {
    'pt-br': 'Email reenviado com sucesso',
    'en-us': 'Resend email successfully'
  },
  sendEmailError: {
    'pt-br': 'Um erro ocorreu ao enviar email',
    'en-us': 'A error occurred to send email'
  },
  emailRecoveryPasswordSuccess: {
    'pt-br': 'Email de recuperação de email enviado',
    'en-us': 'Email of email´s recuperation sended'
  },
  emailInvalid: {
    'pt-br': 'Email invalido',
    'en-us': 'Invalid email'
  },
  emailNotFound: {
    'pt-br': 'Email não encontrado',
    'en-us': 'Email not found'
  },
  emailAlreadyExist: {
    'pt-br': 'Email já existe',
    'en-us': 'Email already exist'
  },
  //============================================================================================
  fileSaveSuccess: {
    'pt-br': 'Imagem salvo',
    'en-us': 'Image saved'
  },
  fileErroCompression: {
    'pt-br': 'Erro na compressão da imagem',
    'en-us': 'Error in image compression'
  },
  fileDeletedSuccess: {
    'pt-br': 'Imagem deletado',
    'en-us': 'Image deleted'
  },
  fileNotExisted: {
    'pt-br': 'Imagem não existe',
    'en-us': 'Image not exist'
  },
  fileParamsIncorrect: {
    'pt-br': 'Parametros incorreto',
    'en-us': 'Params incorrect'
  },
  fileMany: {
    'pt-br': 'Muitas imagens',
    'en-us': 'Params incorrect'
  },
  //============================================================================================
  genderCreatedSuccess: {
    'pt-br': 'Gênero criado com sucesso',
    'en-us': 'Gender created successfully'
  },
  genderCreatedError: {
    'pt-br': 'Erro ao criar gênero',
    'en-us': 'Error by create gender'
  },
  genderDeletedError: {
    'pt-br': 'Erro ao deletar gênero',
    'en-us': 'Error by deleted gender'
  },
  genderDeletedSuccess: {
    'pt-br': 'Gênero deletado com sucesso',
    'en-us': 'Gender deleted successfully'
  },
  //============================================================================================
  plantDeletedSuccess: {
    'pt-br': 'Planta deletada com sucesso',
    'en-us': 'Plant deleted successfully'
  },
  plantNotExist: {
    'pt-br': 'Planta não existe',
    'en-us': 'Plant not found'
  },
  //============================================================================================
  greenhouseTypeCreatedSuccess: {
    'pt-br': 'Tipo da estufa criada com sucesso',
    'en-us': 'Greenhouse\'s type created successfully'
  },
  greenhouseTypeDeletedSuccess: {
    'pt-br': 'Tipo da estuda deletada com sucesso',
    'en-us': 'Greenhouse\'s type deleted successfully'
  },
  greenhouseTypeNotExist: {
    'pt-br': 'Estufa não existe',
    'en-us': 'Greenhouse\'s type not found'
  },
  //============================================================================================
  greenhouseCreatedSuccess: {
    'pt-br': 'Estufa criada com sucesso',
    'en-us': 'Greenhouse created successfully'
  },
  greenhouseDeletedSuccess: {
    'pt-br': 'Estuda deletada com sucesso',
    'en-us': 'Greenhouse deleted successfully'
  },
  greenhouseNotExist: {
    'pt-br': 'Estufa não existe',
    'en-us': 'Greenhouse not found'
  },
  greenhouseUpdatedSuccess: {
    'pt-br': 'Estufa atualizada com sucesso',
    'en-us': 'Greenhouse updated successfully'
  },
  greenhouseLinkedSuccess: {
    'pt-br': 'Estufa adicionada a sua conta com sucesso',
    'en-us': 'Greenhouse added your account successfully'
  },
  greenhouseUnlinkedSuccess: {
    'pt-br': 'Estufa removida da sua conta com sucesso',
    'en-us': 'Greenhouse removed your account successfully'
  },
  greenhouseAlreadyLinked: {
    'pt-br': 'Estufa adicionada em outra conta',
    'en-us': 'Greenhouse added in other account'
  },
  greenhouseAlreadyLinkedYourAccount: {
    'pt-br': 'Estufa adicionada em sua conta',
    'en-us': 'Greenhouse added in your account'
  },
  greenhouseAlreadyUsed: {
    'pt-br': 'Estufa já sendo usada',
    'en-us': 'Greenhouse used in your account'
  },
  //============================================================================================
  plantationCreatedSuccess: {
    'pt-br': 'Plantação criada com sucesso',
    'en-us': 'Plantation creted successfully'
  },
  plantationDeletedSuccess: {
    'pt-br': 'Plantação deleteda com sucesso',
    'en-us': 'Plantação deleted successfully'
  },
  plantationNotExist: {
    'pt-br': 'Plantação não existe',
    'en-us': 'Plantation not found'
  },

  //============================================================================================
}
module.exports.fields = {
  password: {
    'pt-br': 'Senha',
    'en-us': 'Password'
  },
  new_password: {
    'pt-br': 'Senha nova',
    'en-us': 'New password'
  },
  name: {
    'pt-br': 'Nome',
    'en-us': 'Name'
  },
  scientific_name: {
    'pt-br': 'Nome cientifico',
    'en-us': 'Scientific name'
  },
  email: {
    'pt-br': 'Email',
    'en-us': 'Email'
  },
  id_gender: {
    'pt-br': 'Gênero',
    'en-us': 'Gender'
  },
  id_user: {
    'pt-br': 'ID do usuário',
    'en-us': 'User ID'
  },
  is_admin: {
    'pt-br': 'Modo Administrador',
    'en-us': 'Administrator Mode'
  },
  id_plant: {
    'pt-br': 'Planta',
    'en-us': 'Plant'
  },
  birthday_date: {
    'pt-br': 'Data de nascimento',
    'en-us': 'Birthday date'
  },
  num_order: {
    'pt-br': 'Numero de ordem',
    'en-us': 'Order number'
  },
  locale: {
    'pt-br': 'Localidade',
    'en-us': 'Locale'
  },
  value: {
    'pt-br': 'Valor',
    'en-us': 'Value'
  },
  date: {
    'pt-br': 'Data',
    'en-us': 'Date'
  },
  comment: {
    'pt-br': 'Comentário',
    'en-us': 'Comment'
  }
}


module.exports.exceptions = {
  min: {
    'pt-br': '% precisa ter minimo de % caracteres.',
    'en-us': '% need have min of % character.'
  },
  minNumber: {
    'pt-br': '% precisa ser maior que %.',
    'en-us': '% need to be bigger than %.'
  },
  required: {
    'pt-br': '% é necessário.',
    'en-us': '% is needed.'
  },
  oneOf: {
    'pt-br': '% precisa ser: %.',
    'en-us': '% need to be: %.'
  },
  notOneOf: {
    'pt-br': '% não deve ser: %.',
    'en-us': "% don't to be: %."
  },
  valid: {
    'pt-br': '% não é valido.',
    'en-us': "% isn't valid."
  }
}