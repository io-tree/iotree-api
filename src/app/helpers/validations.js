const yup = require('yup');
const { 
	exceptions,
	fields
 } = require('./translate');


yup.setLocale({
	string: {
		min: ({ path, min }) => ({ obj: [ path, min ], exc: exceptions.min }),
		email: ({ path }) => ({ obj: [ path ], exc: exceptions.valid })
	},
	number: {
		min: ({ path, min }) => ({ obj: [ path, min ], exc: exceptions.min }),
		positive: ({ path }) => ({ obj: [ path ], exc: exceptions.valid }),
		moreThan: ({ path, more }) => ({ obj: [ path, more ], exc: exceptions.minNumber })
	},
	mixed: {
		required: ({ path }) => ({ obj: [ path ], exc: exceptions.required }),
		notType: ({ path }) => ({ obj: [ path ], exc: exceptions.valid }),
		oneOf: ({ path, values }) => ({ obj: [ path, values ], exc: exceptions.oneOf }),
		notOneOf: ({ path, values }) => ({ obj: [ path, values], exc: exceptions.notOneOf })
	},
});

module.exports.validate = (schema, value) => {
	try {
		return {
			isValid: true,
			body: schema.validateSync(value, { abortEarly: false})
		}
	} catch(err) {
		//console.log(err)
		let erros =  err.errors.map( error => {
			let aa = Object.entries(error['exc']).map( msg => {
				let text = (error['obj'].map( obj => {
					return msg[1] = msg[1].replace('%', obj);
				}))[error['obj'].length -1 ];
	
				Object.entries(fields).map( field => {
					if(text.match(new RegExp(`(?:^|\\W)${field[0]}(?:$|\\W)`, 'g'))) {
						text = text.replace(field[0], field[1][msg[0]])
					}
				})

				return { [msg[0]]: text }
			})
			return Object.assign(...aa)
		})

		return {
			isValid: false,
			body: erros
		}
	}
}

module.exports.yup = yup;
