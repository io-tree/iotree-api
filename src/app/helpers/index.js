const path = require('path');
const { readdirSync } = require('fs');

let helper = {}
readdirSync(path.join(__dirname))
	.filter( fileName => (fileName !== 'index.js'))
	.forEach(fileName => {
		let name = path.basename(fileName).replace('.js', '');
		helper[name] = require(path.join(__dirname, name))
	});

module.exports = helper;
