
const arrayRandom = [
	['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'],
	['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'],
	['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
];


module.exports = (size, number = true, charMin = false, charMax = false) => {
	let serial = '';
	let sets = [];
	if(number){	sets.push(2); }
	if(charMin) { sets.push(0);	}
	if(charMax) { sets.push(1);	}


	for(let i = 0; i < size; i++){
		let selectSet = sets[Math.floor(Math.random() * sets.length)];
		let selectChar = Math.floor(Math.random() * arrayRandom[selectSet].length);
		serial += (arrayRandom[selectSet][selectChar]);
	}
	return serial;
};
