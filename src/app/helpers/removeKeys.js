

const removeKeys = (obj, fields, separator = '.') => {
	if(!obj) return ;
	let fi = Object.keys(obj);
	fields.map( arr => {
		if(fi.includes(arr)){
			obj[arr] = undefined;
		} else if(arr.indexOf(separator) > -1){
			let nextFields = arr.split(separator);
			let nextKey = nextFields.splice(0,1);
			removeKeys(obj[nextKey], [nextFields.join(separator)]);
		}
	});
	fi.map( arr => {
		if(obj[arr] === null){
			obj[arr] = undefined;
		}
	});
	return obj;
};

module.exports = removeKeys;