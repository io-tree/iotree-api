const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(14);
var CryptoJS = require("crypto-js");


const createHash = (value) => {
	return bcrypt.hashSync(value, salt);
};

const compareHash = (value, hash) => {
	return bcrypt.compareSync(value, hash);
};

const encrypt = (value) => {
	return CryptoJS.AES.encrypt(value, process.env.SECRET_KEY_CRYPT).toString();
}

const decrypt = (value, hash = false) => {
	if(hash)
		value = value.replace(/{/g, '/').replace(/}/g, '\\')
	return (CryptoJS.AES.decrypt(value.toString(), process.env.SECRET_KEY_CRYPT)).toString(CryptoJS.enc.Utf8);
}

const compareCrypt = (value, valueCrypt) => {
	return (CryptoJS.AES.decrypt(valueCrypt.toString(), process.env.SECRET_KEY_CRYPT)).toString(CryptoJS.enc.Utf8) === value;
}

module.exports = {
	createHash,
	compareHash,
	encrypt,
	decrypt,
	compareCrypt
}