const fs = require('fs');
const sharp = require('sharp');
const path = require('path');

const compressImage = (file, size, newName = null) => {
	let newPath = file.path.split('.')[0] + '.webp';
	
	if (newName) {
		newPath = path.dirname(require.main.filename)+'/public/upload/'+newName+'.webp';
	}

	return sharp(file.path)
	.resize(size)
	.toFormat('webp')
	.webp({
		quality: (newPath.match('_small') ? 30 : 80)
	})
	.toBuffer().then(data => {
		try {
			fs.accessSync(file.path);
			fs.unlinkSync(file.path)
		} catch (e) {}

		fs.writeFileSync(newPath, data);

		return '/upload/'+newName+'.webp';
	})
}

const deleteImg = (file_path) => {
	file_path = file_path.path;
	try {
		fs.accessSync(file_path);
		fs.unlinkSync(file_path)
	} catch (e) {
		console.log(e)
	}
}

module.exports = {
	compressImage,
	deleteImg
}