
module.exports = {
	id: 'idGreenhouseType',
	routes: [
		{ method: 'get', router: '/', func: 'getGreenhouseType', notAuth: true },
		{ method: 'post', router: '/', func: 'createGreenhouseType', isAdmin: true },
		{ method: 'delete', router: '/{id}', func: 'deleteGreenhouseType', isAdmin: true },
	]
};
