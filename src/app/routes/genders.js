
module.exports = {
	id: 'idGender',
	routes: [
		{ method: 'get', router: '/', func: 'getGender', notAuth: true },
		{ method: 'post', router: '/', func: 'createGender', isAdmin: true },
		{ method: 'delete', router: '/{id}', func: 'deleteGender', isAdmin: true },
	]
};
