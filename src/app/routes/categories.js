
module.exports = {
  id: 'idCategory',
  routes: [
    { method: 'get', router: '/', func: 'getCategory', notAuth: true },
    { method: 'post', router: '/', func: 'createCategory', isAdmin: true },
    { method: 'put', router: '/{id}', func: 'updateCategory', isAdmin: true },
    { method: 'delete', router: '/{id}', func: 'deleteCategory', isAdmin: true },
  ]
};
