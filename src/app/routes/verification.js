
module.exports = {
	id: 'hash',
	routes: [
		{ method: 'post', router: '/{id}', func: 'reSendEmail', notAuth: true },
		{ method: 'put', router: '/{id}', func: 'verificationEmail', notAuth: true },
	]
};
