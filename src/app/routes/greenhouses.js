module.exports = {
	id: 'idGreenhouse',
	routes: [
		{ method: 'get', router: '/', func: 'getAllGreenhouse', isAdmin: true },
		{ method: 'post', router: '/', func: 'createGreenhouse', isAdmin: true },
		{ method: 'get', router: '/{id}?', func: 'getGreenhouse', dependences: ['users'] },
		{ method: 'post', router: '/{id}?', func: 'setLinkGreenhouse', dependences: ['users'] },
		{ method: 'put', router: '/{id}', func: 'updateGreenhouse', dependences: ['users'] },
	]
};
