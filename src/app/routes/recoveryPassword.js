module.exports = {
	id: 'hash',
	routes: [
		//Rotas de Usuarios
		{ method: 'post', router: '/', func: 'sendRequest', notAuth: true },
		{ method: 'put', router: '/', func: 'changerPassword', notAuth: true },
	]
};
