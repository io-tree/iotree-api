module.exports = {
	id: 'idPlantation',
	routes: [
		{ method: 'get', router: '/{id}?', func: 'getPlantation', dependences: ['users']},
		{ method: 'post', router: '/', func: 'createPlantation', dependences: ['users']},
		{ method: 'put', router: '/{id}', func: 'updatePlantation', dependences: ['users'], notAuth: true},
		{ method: 'delete', router: '/{id}', func: 'deletePlantation', dependences: ['users']},
	]
};
