
module.exports = {
	id: 'idPlant',
	routes: [
		{ method: 'get', router: '/', func: 'getPlant', notAuth: true },
		{ method: 'post', router: '/', func: 'createPlant', isAdmin: true },
		{ method: 'delete', router: '/{id}', func: 'deletePlant', isAdmin: true },
	]
};
