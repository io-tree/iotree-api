
module.exports = {
	id: 'idReconnect',
	routes: [
		{ method: 'post', router: '/', func: 'reconnect', notAuth: true },
	]
};
