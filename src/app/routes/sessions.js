module.exports = {
	id: 'idUser',
	routes: [
		{ method: 'post', router: '/', func: 'loginUser', notAuth: true },
		{ method: 'post', router: '/greenhouse/:idGreenhouse', func: 'loginGreenhouse', notAuth: true },
	]
};
