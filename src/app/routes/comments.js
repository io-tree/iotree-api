
module.exports = {
	id: 'idComment',
	routes: [
		{ method: 'get', router: '/', func: 'getComment', dependences: ['users', 'posts']},
		{ method: 'post', router: '/', func: 'createComment', dependences: ['users', 'posts']},
		{ method: 'put', router: '/{id}', func: 'updateComment', dependences: ['users', 'posts']},
		{ method: 'delete', router: '/{id}', func: 'deleteComment' , dependences: ['users', 'posts']},
	]
};
