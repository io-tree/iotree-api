module.exports = {
	id: 'idUser',
	routes: [
		//Rotas de Usuarios
		{ method: 'get', router: '/{id}', func: 'showUser' },
		{ method: 'post', router: '/', func: 'createUser', notAuth: true },
		{ method: 'put', router: '/{id}', func: 'updateUser' },
		{ method: 'delete', router: '/{id}', func: 'deleteUser' },
		{ method: 'put', router: '/', func: 'setUserAdmin', isAdmin: true },
		{ method: 'get', router: '/', func: 'getAllUser', isAdmin: true },
	]
};
