module.exports = {
	id: 'idPost',
	routes: [
		{ method: 'get', router: '/', func: 'getPost', notAuth: true },
		{ method: 'post', router: '/', func: 'createPost', dependences: ['users'] },
		{ method: 'put', router: '/{id}', func: 'updatePost', dependences: ['users'] },
		{ method: 'delete', router: '/{id}', func: 'deletePost', dependences: ['users'] },
	]
};
