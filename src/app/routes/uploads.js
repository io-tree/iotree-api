module.exports = {
  id: 'idUpload',
  routes: [
    { method: 'post', router: '/', func: 'createArq', upload: true, notAuth: true },
    { method: 'delete', router: '/', func: 'deleteArq', notAuth: true },
  ]
};
