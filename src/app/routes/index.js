const path = require('path');
const {
	readdirSync
} = require('fs');
const {
	pathRoutes,
	pathControllers,
	pathApi
} = require('../../config/paths');
const {
	sendResponse
} = require('../middleware/result');
const { auth } = require('../middleware/auth');
const { multer } = require('../middleware/multer');
const { pre_config } = require('../middleware/pre_config');
const { adminRoute } = require('../middleware/admin');

module.exports = (app) => {
	readdirSync(path.join(__dirname, pathRoutes))
		.filter(fileName => (fileName !== 'index.js'))
		.forEach(fileName => {
			let name = '/' + path.basename(fileName);
			(process.env.NODE_ENV === 'development' && console.log('\n', "=================================== " + fileName));
			try {
				let router = require('express').Router();
				const file = require(path.join(__dirname, pathRoutes, name));
				file.routes.map((arr) => {

					if (process.env.NODE_ENV === 'production' && arr.test) return;

					let midd = [
						name.split('.')[0] + arr.router.replace('{id}', ':' + file.id),
						auth,
						require(path.join(__dirname, pathControllers, name))[arr.func],
						sendResponse
					];

					if (arr.dependences) {
						let temp = '/'
						arr.dependences.map(dep => {
							temp = temp + dep + "/:" + require(path.join(__dirname, pathRoutes, dep + '.js')).id + '/'
						})
						midd[0] = temp.substring(0, temp.length - 1) + midd[0]
					}

					if (arr.upload) {
						midd.splice(2, 0, multer.array('img'));
					}

					if (arr.notAuth) {
						midd.splice(midd.indexOf(auth), 1);
					}

					if (arr.isAdmin) {
						midd.splice(2, 0, adminRoute);
					}

					midd.splice(1, 0, pre_config);
					router[arr.method](...midd);

					(process.env.NODE_ENV === 'development' && console.log(midd));
				});
				app.use(pathApi, router);
			} catch (e) {
				console.log(e);
				console.log({
					error: 'Algo de errado na rota: ' + name
				});
			}
		});
};