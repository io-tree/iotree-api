module.exports = {
	id: 'idReact',
	routes: [
		{ method: 'post', router: '/', func: 'createReact', dependences: ['users']},
		{ method: 'delete', router: '/', func: 'deleteReact', dependences: ['users'] },
	]
};
