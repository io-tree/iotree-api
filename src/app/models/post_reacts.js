module.exports = (sequelize, DataTypes) => {
	const post_reacts = sequelize.define('post_reacts', {
		id_react: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		id_post: DataTypes.UUID,
		id_user: DataTypes.UUID,
		id_react_type: DataTypes.UUID,
	});

	post_reacts.associate = (models) => {
		post_reacts.belongsTo(models.posts, {
			foreignKey: 'id_post',
			targetKey: 'id_post',
			as: 'posts',
		});
		post_reacts.hasMany(models.react_types, {
			foreignKey: 'id_react_type',
			sourceKey: 'id_react_type',
			as: 'reacts',
		});
	}

	return post_reacts;
};