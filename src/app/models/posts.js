module.exports = (sequelize, DataTypes) => {
	const posts = sequelize.define('posts', {
		id_post: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		id_user: DataTypes.UUID,
		title: DataTypes.STRING,
		description: DataTypes.TEXT,
		path_img: DataTypes.STRING,
	});

	posts.associate = (models) => {
		posts.belongsTo(models.users, {
			foreignKey: 'id_user',
			targetKey: 'id_user',
			as: 'users',
		});
		posts.hasMany(models.comments, {
			foreignKey: 'id_post',
			sourceKey: 'id_post',
			as: 'comments',
			onDelete: 'CASCADE',
			hooks: true
		});
		posts.hasMany(models.post_reacts, {
			foreignKey: 'id_post',
			sourceKey: 'id_post',
			as: 'reacts',
			onDelete: 'CASCADE',
			hooks: true
		})
	}

	return posts;
};