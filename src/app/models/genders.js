module.exports = (sequelize, DataTypes) => {
	const genders = sequelize.define('genders', {
		id_gender: DataTypes.INTEGER,
		name: DataTypes.STRING,
		num_order: DataTypes.INTEGER
	});

	genders.removeAttribute('id');

	return genders;
};