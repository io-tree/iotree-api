module.exports = (sequelize, DataTypes) => {
	const react_types = sequelize.define('react_types', {
		id_react_type: DataTypes.INTEGER,
		name: DataTypes.STRING,
		path_icon: DataTypes.STRING,
	});

	/*

		happy
		faSmile
		faGrinSquintTears

		like
		faThumbsUp
		faHeart

		sad
		faFrow
		faSadTear
		faSadCry

		rage
		faAngry
	*/

	react_types.removeAttribute('id');


	return react_types;
};