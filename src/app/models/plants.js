module.exports = (sequelize, DataTypes) => {
  const plants = sequelize.define('plants', {
    id_plant: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    name: DataTypes.STRING,
    scientific_name: DataTypes.STRING,
    description: DataTypes.TEXT,
    humidity_air: {
      type: DataTypes.STRING,
      get: function () {
        const val = this.getDataValue('humidity_air');
        if (val)
          return JSON.parse(val);
      },
      set: function (value) {
        this.setDataValue('humidity_air', JSON.stringify(value));
      },
    },
    humidity_ground: {
      type: DataTypes.STRING,
      get: function () {
        const val = this.getDataValue('humidity_ground');
        if (val)
          return JSON.parse(val);
      },
      set: function (value) {
        this.setDataValue('humidity_ground', JSON.stringify(value));
      },
    },
    temperature: {
      type: DataTypes.STRING,
      get: function () {
        const val = this.getDataValue('temperature');
        if (val)
          return JSON.parse(val);
      },
      set: function (value) {
        this.setDataValue('temperature', JSON.stringify(value));
      },
    },
    pH: {
      type: DataTypes.STRING,
      get: function () {
        const val = this.getDataValue('pH');
        if (val)
          return JSON.parse(val);
      },
      set: function (value) {
        this.setDataValue('pH', JSON.stringify(value));
      },
    },
    height: {
      type: DataTypes.STRING,
      get: function () {
        const val = this.getDataValue('height');
        if (val)
          return JSON.parse(val);
      },
      set: function (value) {
        this.setDataValue('height', JSON.stringify(value));
      },
    },
    cycle: {
      type: DataTypes.STRING,
      get: function () {
        const val = this.getDataValue('cycle');
        if (val)
          return JSON.parse(val);
      },
      set: function (value) {
        this.setDataValue('cycle', JSON.stringify(value));
      },
    },
    id_category: DataTypes.INTEGER,
    family: DataTypes.STRING,
    origin: DataTypes.STRING,
    luminosity: DataTypes.STRING,
    weather: DataTypes.STRING,
    path_img: DataTypes.STRING,
  });

  plants.associate = (models) => {
    plants.hasOne(models.categories, {
      foreignKey: 'id_category',
      sourceKey: 'id_category',
      as: 'category',
    })
  }

  plants.beforeUpdate((plant, options) => {
    plant.path_img === null ? plant.path_img = '/upload/default-plant.png' : plant.path_img = plant.path_img;
  });

  return plants;
};