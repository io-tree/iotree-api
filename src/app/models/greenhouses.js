const { crypt } = require('../helpers');

module.exports = (sequelize, DataTypes) => {
	const greenhouses = sequelize.define('greenhouses', {
		id_greenhouse: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true
		},
		id_user: DataTypes.UUID,
		code: {
			type: DataTypes.STRING,
			get: function () {
				const val = this.getDataValue('code');
				if (val)
					return val;
			},
			set: function (value) {
				const uuid = this.getDataValue('id_greenhouse');
				const type = this.getDataValue('id_type');
				let code = uuid.split('-');
				this.setDataValue('code', type + '-' + code[code.length - 1]);
			},
		},
		name: DataTypes.STRING,
		online: DataTypes.BOOLEAN,
		password: DataTypes.STRING,
		id_type: DataTypes.STRING,
	});


	greenhouses.associate = (models) => {
		greenhouses.hasOne(models.greenhouse_types, {
			foreignKey: 'id_greenhouse_type',
			sourceKey: 'id_type',
			as: 'greenhouse_type',
		});

		greenhouses.belongsTo(models.plantations, {
			foreignKey: 'id_greenhouse',
			targetKey: 'id_greenhouse',
			as: 'plantation',
		})
	}

	greenhouses.beforeCreate((greenhouse, options) => {
		greenhouse.password = crypt.createHash(greenhouse.password);
	})

	return greenhouses;
};