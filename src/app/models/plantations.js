module.exports = (sequelize, DataTypes) => {
  const plantations = sequelize.define('plantations', {
    id_plantation: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    id_plant: DataTypes.UUID,
    id_greenhouse: DataTypes.UUID,
    id_user: DataTypes.UUID,
    is_done: DataTypes.BOOLEAN,
    temperature: DataTypes.STRING,
    humidity_air: DataTypes.STRING,
    humidity_ground: DataTypes.STRING,
    time_irrigation: DataTypes.DATE,
    done_time: DataTypes.DATE,
  });

  plantations.associate = (models) => {
    plantations.hasOne(models.greenhouses, {
      foreignKey: 'id_greenhouse',
      sourceKey: 'id_greenhouse',
      as: 'greenhouses',
    });
    plantations.hasOne(models.plants, {
      foreignKey: 'id_plant',
      sourceKey: 'id_plant',
      as: 'plants',
    });
    plantations.hasOne(models.users, {
      foreignKey: 'id_user',
      sourceKey: 'id_user',
      as: 'users',
    });
  }

  return plantations;
};