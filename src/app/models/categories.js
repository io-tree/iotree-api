module.exports = (sequelize, DataTypes) => {
	const categories = sequelize.define('categories', {
		id_category: DataTypes.INTEGER,
		name: DataTypes.STRING,
		description: DataTypes.TEXT,
		num_order: DataTypes.INTEGER,
		path_img: DataTypes.STRING
	});

	categories.removeAttribute('id');

	return categories;
};