const fs = require('fs');
const path = require('path');
const database = require('../../database/index');
const models = {};
const pathRoutes = path.join(__dirname, '../models');


fs.readdirSync(pathRoutes)
	.filter(file => (file.indexOf('.') !== 0) && (file !== path.basename(__filename)) && (file.slice(-3) === '.js') && (file !== 'index.js'))
	.forEach((file) => {
		const model = database.import(path.join(pathRoutes, file));
		models[model.name] = model;
		(process.env.NODE_ENV === 'development' && console.log(model));
	});

Object.keys(models).forEach(modelName => {
	if (models[modelName].associate) {
		models[modelName].associate(models);
	}
});


models.Op = database.Op;
models.sequelize = database.sequelize;
module.exports = models;
