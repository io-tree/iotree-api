module.exports = (sequelize, DataTypes) => {
	const comments = sequelize.define('comments', {
		id_comment: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		id_user: DataTypes.UUID,
		id_post: DataTypes.UUID,
		in_comment: DataTypes.UUID,
		text: DataTypes.TEXT,
	});

	comments.associate = (models) => {
		comments.belongsTo(models.users, {
			foreignKey: 'id_user',
			targetKey: 'id_user',
			as: 'users',
		});
		comments.belongsTo(models.posts, {
			foreignKey: 'id_post',
			targetKey: 'id_post',
			as: 'posts',
		});
		comments.hasMany(models.comments, {
			foreignKey: 'id_comment',
			sourceKey: 'in_comment',
			as: 'comments',
			onDelete: 'CASCADE',
			hooks: true
		});
		comments.hasMany(models.comment_reacts, {
			foreignKey: 'id_comment',
			sourceKey: 'id_comment',
			as: 'reacts',
			onDelete: 'CASCADE',
			hooks: true
		})
	}

	return comments;
};