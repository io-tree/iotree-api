const {
	crypt
} = require('../helpers');

module.exports = (sequelize, DataTypes) => {
	const users = sequelize.define('users', {
		id_user: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING(80),
			validate: {
				is: ['[A-Za-z]{3,80}', 'i'],
				notEmpty: true,
				notNull: false,
			}
		},
		birthday_date: {
			type: DataTypes.DATEONLY,
			validate: {
				isDate: true
			}
		},
		id_gender: DataTypes.INTEGER,
		email: DataTypes.STRING,
		password: DataTypes.STRING,
		path_img: DataTypes.STRING,
		is_enabled: DataTypes.BOOLEAN,
		is_admin: DataTypes.BOOLEAN,
		login_attempts: DataTypes.INTEGER,
		blockedAt: DataTypes.DATE,
		passwordChangedAt: DataTypes.DATE,
	});

	users.associate = (models) => {
		users.hasMany(models.posts, {
			foreignKey: 'id_user',
			sourceKey: 'id_user',
			as: 'comments',
			onDelete: 'CASCADE',
			hooks: true
		});

		users.hasMany(models.plantations, {
			foreignKey: 'id_user',
			sourceKey: 'id_user',
			as: 'plantations',
			onDelete: 'CASCADE',
			hooks: true
		});

		users.hasOne(models.genders, {
			foreignKey: 'id_gender',
			sourceKey: 'id_gender',
			as: 'gender',
			onDelete: 'SET NULL',
			hooks: true
		});

		users.hasMany(models.greenhouses, {
			foreignKey: 'id_user',
			sourceKey: 'id_user',
			as: 'greenhouses',
		});
	}


	users.beforeCreate((user, options) => {
		user.password = crypt.createHash(user.password);
	})

	users.beforeUpdate((user, options) => {
		user.path_img === null ? user.path_img = '/upload/default-user.png' : user.path_img = user.path_img;
	});

	return users;
};