module.exports = (sequelize, DataTypes) => {
	const comment_reacts = sequelize.define('comment_reacts', {
		id_comment_react: {
			type: DataTypes.UUID,
			defaultValue: DataTypes.UUIDV4,
			primaryKey: true,
		},
		id_comment: DataTypes.UUID,
		id_user: DataTypes.UUID,
		id_react_type: DataTypes.UUID,
	});

	comment_reacts.associate = (models) => {
		comment_reacts.belongsTo(models.comments, {
			foreignKey: 'id_comment',
			targetKey: 'id_comment',
			as: 'comment_reacts',
		});
		comment_reacts.hasOne(models.react_types, {
			foreignKey: 'id_react_type',
			sourceKey: 'id_react_type',
			as: 'reacts',
		});
	}

	return comment_reacts;
};