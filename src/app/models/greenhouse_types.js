module.exports = (sequelize, DataTypes) => {
	const greenhouse_types = sequelize.define('greenhouse_types', {
		id_greenhouse_type: DataTypes.STRING,
		name: DataTypes.STRING,
		num_order: DataTypes.INTEGER,
		description: DataTypes.TEXT
	});

	greenhouse_types.removeAttribute('id');

	return greenhouse_types;
};