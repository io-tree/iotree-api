require('dotenv/config');

const express = require('express');
const app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
const cors = require('cors');
const morgan = require('morgan');
const routes = require('./src/app/routes');
const scheduler = require('./src/app/schedulers');
const {socket} = require('./src/app/controllers/socket');
const {
	pathApi
} = require('./src/config/paths');

//middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));
app.use(pathApi, express.static('public'));
app.use(morgan(':method :url :status :response-time ms'));

app.get(pathApi, (req, res) => {
	const status = {
		status: 'ok'
	};
	if (process.env.NODE_ENV === 'development') {
		status.env = process.env;
		status.host = req.hostname;
	}
	res.send(status);
});

socket(io);


//init
routes(app); //Inicia as rotas
scheduler(); //inicia o temporizador

app.use('*', function (req, res) {
	res.status(404).json({ error: 'Route not found' });
});

module.exports = {
	server
};