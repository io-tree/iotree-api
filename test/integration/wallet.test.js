const app = require('../../app');
const request = require('supertest');
const { sequelize } = require('../../src/database');
const faker = require('faker');
const random = require('../../src/helpers/random');
const { pathApi } = require('../../src/config/paths');
let userTest = [], dependentTest = [];

const registerPath = pathApi+'/auth/users';
const dependentPath = pathApi+'/users/dependents';
const walletPath = pathApi+'/wallets';

beforeAll( async () => {
	userTest.push({
		name: faker.name.findName(),
		email: faker.internet.email(),
		password: faker.internet.password(),
		birthday_date: faker.date.past(),
		genre: 'M',
		locale: "pt-br"
	});
	userTest.push({
		name: faker.name.findName(),
		email: faker.internet.email(),
		password: faker.internet.password(),
		birthday_date: faker.date.past(),
		genre: 'F',
		locale: "en-us"
	});
	
	dependentTest.push({
		name: faker.name.findName(),
		genre: "M",
		password: random(6)
	});
	dependentTest.push({
		name: faker.name.findName(),
		genre: "F",
		password: random(6)
	});
	let temp;
	temp = (await request(app).post(registerPath).send(userTest[0])).body;
	userTest[0].token = temp.token;
	userTest[0].id= temp.msg.id_user;
	
	temp = (await request(app).post(registerPath).send(userTest[1])).body;
	userTest[1].token = temp.token;
	userTest[1].id= temp.msg.id_user;
	
	temp = (await request(app).post(dependentPath).set('authorization','Bearer '+ userTest[0].token).send(dependentTest[0])).body;
	dependentTest[0].token = temp.token;
	
	temp = (await request(app).post(dependentPath).set('authorization','Bearer '+ userTest[1].token).send(dependentTest[1])).body;
	dependentTest[1].token = temp.token;
});

afterAll(async done => {
	sequelize.close();
	done();
});

describe('Add money in wallets fo users', () => {
	it('add Money in users 0', async () => {
		const user = (await request(app)
			.put(walletPath+'/deposit')
			.set('authorization','Bearer '+ userTest[0].token)
			.send({
				value: "1000"
			})
		).body;
		
		expect(user).toHaveProperty('status','success');
	});
	
	it('add Money in users 1', async () => {
		const user = (await request(app)
				.put(walletPath+'/deposit')
				.set('authorization','Bearer '+ userTest[1].token)
				.send({
					value: 1000
				})
		).body;
		
		expect(user).toHaveProperty('status','success');
	});
	
});

