const app = require('../../app');
const request = require('supertest');
const { sequelize } = require('../../src/database');
const faker = require('faker');
const random = require('../../src/helpers/random');
const { pathApi } = require('../../src/config/paths');
let userTest, userData;
let dependentTest;
let token;

const registerPath = pathApi+'/auth/users';
const dependentPath = pathApi+'/users/dependents';

beforeAll( async () => {
	userData = {
		name: faker.name.findName(),
		email: faker.internet.email(),
		password: faker.internet.password(),
		birthday_date: faker.date.past(),
		gender: 'M',
		locale: "pt-br"
	};
	
	userTest = (await request(app)
		.post(registerPath)
		.send(userData)
	).body;
	
	token = userTest.token;
	//userTest = userTest.msg;
});

afterAll(async done => {
	sequelize.close();
	done();
});

describe('Register dependent', () => {
	it('should create dependent in userTest', async () => {
		const dependent = (await request(app)
			.post(dependentPath)
			.set('authorization','Bearer '+token)
			.send({
				name: faker.name.findName(),
				genre: "M",
				password: random(6)
			})
		).body;
		
		dependentTest = dependent.msg;
		
		expect(dependent).toHaveProperty('status', 'success');
	});
	
	it("shouldn't create dependent in userTest within name", async () => {
		const dependent = (await request(app)
			.post(dependentPath)
			.set('authorization','Bearer '+token)
			.send({
				//name: faker.name.findName(),
				genre: "M",
				password: random(6)
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'error');
	});
	
	it("shouldn't create dependent in userTest within genre", async () => {
		const dependent = (await request(app)
			.post(dependentPath)
			.set('authorization','Bearer '+token)
			.send({
				name: faker.name.findName(),
				//genre: "M",
				password: random(6)
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'error');
	});
	
	it("shouldn't create dependent in userTest within password", async () => {
		const dependent = (await request(app)
			.post(dependentPath)
			.set('authorization','Bearer '+token)
			.send({
				name: faker.name.findName(),
				genre: "M",
				//password: random(6)
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'error');
	});
	
	it("shouldn't create dependent in userTest with password less than 6 character", async () => {
		const dependent = (await request(app)
			.post(dependentPath)
			.set('authorization','Bearer '+token)
			.send({
				name: faker.name.findName(),
				genre: "M",
				password: random(5)
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'error');
	});
});

describe('Update Dependent', () => {
	it('should update all fields', async () => {
		const dependent = (await request(app)
			.put(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send({
				name: 'Dependent update test',
				genre: 'F',
				password_new: '123456789',
				password: userData.password
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'success');
		expect(dependent).toHaveProperty('msg');
	});
	
	it('should update just name', async () => {
		const dependent = (await request(app)
			.put(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send({
				name: userData.name,
				password: userData.password
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'success');
		expect(dependent.msg.name).toBe(userData.name);
	});
	
	it('should update just genre', async () => {
		const dependent = (await request(app)
			.put(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send({
				genre: userData.genre,
				password: userData.password,
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'success');
		expect(dependent.msg.genre).toBe(userData.genre);
	});
	
	it('should update just password', async () => {
		const dependent = (await request(app)
			.put(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send({
				password_new: '123456789',
				password: userData.password
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'success');
	});
	
	it("shouldn't update with wrong password", async () => {
		const dependent = (await request(app)
			.put(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send({
				password_new: '123456789',
				password: random(6)
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'error');
	});
	
	
	it("shouldn't update with password less than 6", async () => {
		const dependent = (await request(app)
			.put(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send({
				password_new: random(5),
				password: userData.password
			})
		).body;
		
		expect(dependent).toHaveProperty('status', 'error');
	});
});

describe('Delete dependent and your tutor', () => {
	it("should delete dependent", async () => {
		const dependent = (await request(app)
			.delete(dependentPath+'/'+dependentTest.id_user)
			.set('authorization','Bearer '+token)
			.send()
		).body;
		
		expect(dependent).toHaveProperty('status','success');
	});
	
	it('should delete dependent tutor', async () => {
		const user = (await request(app)
			.delete(pathApi+'/users')
			.set('authorization','Bearer '+token)
			.send()
		).body;
		
		expect(user).toHaveProperty('status','success');
	});
});